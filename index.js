const http = require("http");
const express = require("express");
const inforUser = require("./models/users");
const gameModel = require("./models/game");
const mongoose = require("mongoose");
const nodemailer = require("nodemailer");
const timeModel = require("./models/timespents");
const timeModelLogins = require("./models/loginstasts");
const Oneusers = require("./models/userones")
const Likesuser = require("./models/likescounts");
const ics = require("ics");
const jwt = require("jsonwebtoken");
var cors = require("cors");

mongoose.set("useFindAndModify", false);

const bodyParser = require("body-parser");


const app = express();


app.use(
  bodyParser.urlencoded({
    extended: false,
  })
);


// parse application/json
app.use(bodyParser.json());

//app.use(cors());
var port = process.env.PORT || 3001;

var jsonParser = bodyParser.json();



var urlencodedParser = bodyParser.urlencoded({
  extended: false,
});
const server = http.createServer(app);
//const uri = "mongodb://DbAdmin:AbBviRtuAliVe2022@13.237.141.238:27017/?authSource=admin&readPreference=primary&appname=MongoDB%20Compass&ssl=false";
  // const uri = "mongodb+srv://testing:9kH62kPUDio9OleA@cluster0.kw5yl.mongodb.net/test2?retryWrites=true&w=majority"
const uri = "mongodb://localhost:27017/test"
//const uri =" mongodb://DBAdmin:iFaAcaDemYVirtUALive@3.69.15.12:27017/?authSource=admin&readPreference=primary&appname=MongoDB%20Compass&ssl=false";
mongoose
  .connect(uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })

  .then(() => {
    server.listen(port, () =>
      console.log(` Example app listening on port port!m  and database ${port} ${Date.now()}`)
    );
  })
  .catch((err) => console.log(err));

app.get("/", (req, res) => {
  res.send("Hello World, from express test with22");
});

app.get("/alluser", async (req, res) => {
  let docs = await inforUser.find({});
  console.log(docs)


  res.send({
    count: docs.length, data: docs
  });
});
app.post("/findoneuser", async (req, res) => {
  console.log("sss", req.body);
  let emailtest = await inforUser.findOne({
    userEmail: req.body.userEmail,
  });




  if (emailtest) {
    res.send({
      data: emailtest,
    });
  } else {
    res.status(201).send({
      data: "notfound",
    });
  }

});

app.post("/gamefind", async (req, res) => {
  let docs = await gameModel.find({
    gameName: req.body.gameName,
    email: req.body.email,
  });
  let docsALL = await gameModel
    .find({
      gameName: req.body.gameName,
    })
    .sort({
      userScore: -1,
    })

    .limit(2000);
  var finalData = [];
  console.log("finalData", finalData, docsALL);
  docsALL.map((item, index) => {
    finalData.push({
      gameName: item.gameName,

      userScore: item.userScore,
      name: item.name,
      email: item.email,
      rank: index + 1,
    });
  });

  // console.log("finalData",finalData,docsALL)
  res.send({
    data: {
      docs: docs,
      finalData,
    },
  });
});

app.post("/gamedataenter", async (req, res) => {
  console.log("sss", JSON.stringify(req.body));

  // gameModel.findOneAndUpdate({gameName:req.body.gameName ,phoneNumber:req.body.phoneNumber },null, function (err, docs) {

  let check = await gameModel.find({
    gameName: req.body.gameName,
    email: req.body.email,
  });
  console.log("dataaa", check);
  if (check.length > 0) {


    try {
      if (parseInt(check[0].userScore) < parseInt(req.body.userScore)) {
        gameModel.findOneAndUpdate(
          {
            gameName: req.body.gameName,
            email: req.body.email,
          },
          {
            userScore: req.body.userScore,
          },
          function (err, result) {
            if (err) {
              res.send(err);
            } else {
              res.send(result);
            }
          }
        )

        //   gameModel.updateOne({userScore:parseInt(req.body.userScore)}, function (err, docs) {
        //     if (err){
        //         console.log(err)
        //     }
        //     else{
        //         console.log("Updated Docs : ", docs);
        //         console.log(docs)
        //         return  res.send(docs);
        //     }
        // })
      } else {
        return res.send({
          data: "alredy high score",
        });
      }
    } catch (error) {
      return error;
    }
  } else {
    let newgame = new gameModel({
      gameName: req.body.gameName,

      userScore: parseInt(req.body.userScore),
      name: req.body.name,
      email: req.body.email,
    });
    try {
      let response = await newgame.save();
      res.send(response);
      return response;
    } catch (error) {
      return error;
    }
  }
});

app.post("/mailsend", async (req, res) => {




  if (req.body.confirmpassword !== req.body.password) {
    res.send({
      data: "passwordnotmtach",
    });
  }

  let emailtest = await inforUser.find({
    email: req.body.email,
  });

  if (emailtest.length > 0) {
    res.send({
      data: "alredyRegister",
    });
  }

  if (emailtest.length > 0) {
    inforUser.findOneAndUpdate(
      {
        email: req.body.email,
      },
      {
        password: req.body.password,
        register: "register",
      },
      function (err, result) {
        if (err) {
          res.send(err);
        } else {
          res.send({
            data: "changed",
          });
        }
      }
    );
  } else {
    res.send({
      data: "not",
    });
  }

  if (emailtest.length > 0) {
    // async..await is not allowed in global scope, must use a wrapper
    async function main() {
      // Generate test SMTP service account from ethereal.email
      // Only needed if you don't have a real mail account for testing
      let testAccount = await nodemailer.createTestAccount();

      // create reusable transporter object using the default SMTP transport
      let transporter = nodemailer.createTransport({
        service: "gmail",
        port: 587,
        secure: false, // true for 465, false for other ports
        auth: {
          user: "noreply@quest-emm.virtuallive.in",
          pass: "sppl2014",
        },
      });

      // send mail with defined transport object
      let info = await transporter.sendMail({
        from: req.body.email, // sender address
        to: `${req.body.email}`, // list of receivers
        subject: "Hello ✔", // Subject line
        text: "Hello world?", // plain text body
        html: `<br>Hi ${req.body.email} <br> your password is ${req.body.password}  `, // html body
      });

      console.log("Message sent: %s", info.messageId);
      // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

      // Preview only available when sending through an Ethereal account
      console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));

      // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
    }

    main().catch(console.error);
  }
});

app.post("/registration", async (req, res) => {

console.log("req",req.body)

  let emailtest = await inforUser.findOne({
    userEmail: req.body.userEmail,
  });

  if (emailtest) {
    res.status(200).send({
      data: 'already',
    });
  }
  else {
   
    var newUser = new inforUser({

      firstName: req.body.firstName,
      lastName: req.body.lastName,
      userEmail:req.body.userEmail,
      userCountryName:req.body.userCountryName,
      mobileNumber:req.body.mobileNumber,
      telCode:req.body.telCode,
      timeZone:req.body.timeZone,
      companyName:req.body.companyName,
      dob:req.body.dob,
      bType:req.body.bType
    })

   


const TheUrlMail=(req.body.timeZone=="For USA & Latin America / Thursday 7 April 2022"||req.body.timeZone=="For Europe-Africa-Asia-India-Australia / Friday 8 April 2022"?'https://ifaacademy.mykajabi.com/vip-session-conference-2022':'https://panagiotis-leledakis.mykajabi.com/gr-vip-session-conference')
 

if(req.body.timeZone=="For USA & Latin America / Thursday 7 April 2022")
{
   let TheUrl= (req.body.timeZone=="For USA & Latin America / Thursday 7 April 2022"||req.body.timeZone=="For Europe-Africa-Asia-India-Australia / Friday 8 April 2022"?'https://ifaacademy.mykajabi.com/vip-session-conference-2022':'https://panagiotis-leledakis.mykajabi.com/gr-vip-session-conference')
   

   let SENDGRID_API_KEY =
   "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";
   let sgMail = require("@sendgrid/mail");
   sgMail.setApiKey(SENDGRID_API_KEY);
   console.log("europe")
    const msg = {
      to: req.body.userEmail, // Change to your recipient
      from: "noreply@ifaacademy.virtuallive.in", // Change to your verified sender
      subject: "`Thanks For resgister ",
      html: `<!DOCTYPE html>
      <html lang="en" >
      <head>
        <meta charset="UTF-8">
        <title>IFA Academy</title>
      
      
      </head>
      <body>
      <!-- partial:index.partial.html -->
      <div data-template-type="html" style="height: auto; padding-bottom: 149px;" class="ui-sortable">
         <!--[if !mso]><!-->
         <!--<![endif]-->
         <!-- ====== Module : Intro ====== -->
         <table bgcolor="#F5F5F5" align="center" class="full" border="0" cellpadding="0" cellspacing="0" data-thumbnail="http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2020/03/22/9jhlVUzudcB8NtbnMg67SvA5/StampReady/thumbnails/thumb-1.png" data-module="Module-1" data-bgcolor="M1 Bgcolor 1">
            <tbody>
               <tr>
                  <td height="25" style="font-size:0px">&nbsp;</td>
               </tr>
               <!-- top -->
               <tr>
                  <td>
                     <table align="center" class="res-full ui-resizable" border="0" cellpadding="0" cellspacing="0">
                        <tbody>
                           <tr>
                              <td>
                                 <table align="center" border="0" cellpadding="0" cellspacing="0">
                                    <tbody>
                                       <tr>
                                          <td valign="middle" style="vertical-align: middle;">
                                             <img width="18" style="max-width: 120px; width: 100%; display: block; line-height: 0px; font-size: 0px; border: 0px;" 
                                             src="https://test.virtuallive.in/ifa-logo-01.png">
                                          </td>
                                          <td width="10"></td>
                                          <td valign="middle" style="vertical-align: middle;">
                                             <table align="center" border="0" cellpadding="0" cellspacing="0">
                                                <!-- link -->
                                                <tbody>
                                                   <tr>
                                                    
                                                   </tr>
                                                   <!-- link end -->
                                                </tbody>
                                             </table>
                                          </td>
                                       </tr>
                                    </tbody>
                                 </table>
                              </td>
                           </tr>
                        </tbody>
                        <div class="ui-resizable-handle ui-resizable-s" style="z-index: 90;"></div>
                     </table>
                  </td>
               </tr>
               <!-- top end -->
               <tr>
                  <td height="25" style="font-size:0px">&nbsp;</td>
               </tr>
               <tr>
                  <td>
                     <table bgcolor="#304050" align="center"  width="900" height="400" class="margin-full ui-resizable" style="background-size: cover; background-position: center center; background-size: cover; border-radius: 6px 6px 0px 0px; background-image: 
                     url(&quot;https://test.virtuallive.in/usa.jpg&quot;);" border="0" cellpadding="0" cellspacing="0" background="#" data-bgcolor="M1 Bgcolor 2" data-background="M1 Background 1">
                        <tbody>
                           <tr>
                              <td>
                                 <table width="600" align="center" class="margin-pad" border="0" cellpadding="0" cellspacing="0">
                                    <tbody>
                                       <tr>
                                          <td height="70" style="font-size:0px">&nbsp;</td>
                                       </tr>
                                       <!-- image -->
                                       <tr>
                                          <td>
                                             <table align="center" class="res-full" border="0" cellpadding="0" cellspacing="0">
                                                <tbody>
                                                   <tr>
                                                      <td>
                                                         <table align="center" border="0" cellpadding="0" cellspacing="0">
                                                            <tbody>
                                                               <tr>
                                                                  <!-- <td>
                                                                     <img width="65" style="max-width: 65px; width: 100%; display: block; line-height: 0px; font-size: 0px; border: 0px;" src="http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2020/03/22/9jhlVUzudcB8NtbnMg67SvA5/StampReady/img/module01-img02.png">
                                                                  </td> -->
                                                               </tr>
                                                            </tbody>
                                                         </table>
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                          </td>
                                       </tr>
                                       <!-- image end -->
                                       <tr>
                                          <td height="45" style="font-size:0px">&nbsp;</td>
                                       </tr>
                                       <!-- title -->
                                       <tr>
                                       
                                       </tr>
                                       <!-- title end -->
                                       <tr>
                                          <td height="12" style="font-size:0px">&nbsp;</td>
                                       </tr>
                                       <!-- title -->
                                       <tr>
                                        
                                       </tr>
                                       <!-- title end -->
                                       <tr>
                                          <td height="30" style="font-size:0px" class="">&nbsp;</td>
                                       </tr>
                                       <tr>
                                          <td>
                                             <table align="center" border="0" cellpadding="0" cellspacing="0">
                                                <tbody>
                                                   <tr>
                                                      <td>
                                                         <table align="center" border="0" cellpadding="0" cellspacing="0">
                                                            <!-- link -->
                                                            <tbody>
                                                               <tr>
                                                               
                                                               </tr>
                                                               <!-- link end -->
                                                            </tbody>
                                                         </table>
                                                      </td>
                                                      <td style="padding: 0 12px; color: #798999;">
                                                         •
                                                      </td>
                                                      <td>
                                                         <table align="center" border="0" cellpadding="0" cellspacing="0">
                                                            <!-- link -->
                                                            <tbody>
                                                               <tr>
                                                                  <!-- <td class="res-center" style="text-align: center;">
                                                                     <a href="https://example.com" style="color: white; font-family: 'Nunito', Arial, Sans-serif; font-size: 16px; letter-spacing: 0.7px; text-decoration: none; word-break: break-word;" data-color="M1 Link 3" data-size="M1 Link 3" data-max="26" data-min="6">
                                                                     Webpage
                                                                     </a>
                                                                  </td> -->
                                                               </tr>
                                                               <!-- link end -->
                                                            </tbody>
                                                         </table>
                                                      </td>
                                                      <td style="padding: 0 12px; color: #798999;">
                                                         •
                                                      </td>
                                                      <td>
                                                         <table align="center" border="0" cellpadding="0" cellspacing="0">
                                                            <!-- link -->
                                                            <tbody>
                                                               <tr>
                                                                  <!-- <td class="res-center" style="text-align: center;">
                                                                     <a href="https://example.com" style="color: white; font-family: 'Nunito', Arial, Sans-serif; font-size: 16px; letter-spacing: 0.7px; text-decoration: none; word-break: break-word;" data-color="M1 Link 4" data-size="M1 Link 4" data-max="26" data-min="6">
                                                                     Contact
                                                                     </a>
                                                                  </td> -->
                                                               </tr>
                                                               <!-- link end -->
                                                            </tbody>
                                                         </table>
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                          </td>
                                       </tr>
                                       <tr>
                                          <td height="70" style="font-size:0px">&nbsp;</td>
                                       </tr>
                                    </tbody>
                                 </table>
                              </td>
                           </tr>
                        </tbody>
                        <div class="ui-resizable-handle ui-resizable-s" style="z-index: 90;"></div>
                     </table>
                  </td>
               </tr>
            </tbody>
         </table>
         <!-- ====== Module : Texts ====== -->
         <table bgcolor="#F5F5F5" align="center" class="full selected-table" border="0" cellpadding="0" cellspacing="0" data-thumbnail="http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2020/03/22/9jhlVUzudcB8NtbnMg67SvA5/StampReady/thumbnails/thumb-2.png" data-module="Module-2" data-bgcolor="M2 Bgcolor 1">
            <tbody>
               <tr>
                  <td>
                     <table bgcolor="white" width="750" align="center" class="margin-full ui-resizable" border="0" cellpadding="0" cellspacing="0" data-bgcolor="M2 Bgcolor 2">
                        <tbody>
                           <tr>
                              <td>
                                 <table width="600" align="center" class="margin-pad" border="0" cellpadding="0" cellspacing="0">
                                    <tbody>
                                       <tr>
                                          <td height="70" style="font-size:0px">&nbsp;</td>
                                       </tr>
                                       <!-- HEADING -->
                                       <!-- subtitle -->
                                       <tr>
                                          <td class="res-center" style="text-align: left; color: #000; font-family: 'Raleway', Arial, Sans-serif; font-size: 14px; letter-spacing: 1px; word-break: break-word; font-weight: 800;" data-color="M2 Subtitle 1" data-size="M2 Subtitle 1" data-max="24" data-min="5">
                                            Thank you so much!
                                          </td>
                                       </tr>
                                       <!-- subtitle end -->
                                       <tr>
                                          <td height="13" style="font-size:0px">&nbsp;</td>
                                       </tr>
                                       <!-- title -->
                                       <tr>
                                          <td class="res-center selected-element" style="text-align: left; color: #000; font-family: 'Raleway', Arial, Sans-serif; font-size: 15px; letter-spacing: 0.7px; word-break: break-word" data-color="M2 Title 1" data-size="M2 Title 1" data-max="32" data-min="12" contenteditable="true">
                                            We are so excited you've decided to attend our annual conference of 2022. <br>
      We can't wait to share our best ideas and tips and interact with you.
      
                                          </td>
                                       </tr>
                                       <!-- title end -->
                                       <tr>
                                          <td height="15" style="font-size:0px">&nbsp;</td>
                                       </tr>
                                       <!-- image -->
                                       <tr>
                                          <td>
                                             <table align="center" class="res-full" border="0" cellpadding="0" cellspacing="0">
                                                <tbody>
                                                   <tr>
                                                      <td>
                                                         <table align="center" border="0" cellpadding="0" cellspacing="0">
                                                            <tbody>
                                                               <tr>
                                                                  <!-- <td>
                                                                     <img width="130" style="max-width: 130px; width: 100%; display: block; line-height: 0px; font-size: 0px; border: 0px;" src="http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2020/03/22/9jhlVUzudcB8NtbnMg67SvA5/StampReady/img/ui-line-2.png">
                                                                  </td> -->
                                                               </tr>
                                                            </tbody>
                                                         </table>
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                          </td>
                                       </tr>
                                       <!-- image end -->
                                       <!-- HEADING end -->
                                       <tr>
                                          <td height="30" style="font-size:0px">&nbsp;</td>
                                       </tr>
                                       <!-- paragraph -->
                                       <tr>
                                          <td class="res-center" style="text-align: center; color: #000; font-family: 'Nunito', Arial, Sans-serif; font-size: 16px; letter-spacing: 0.4px; line-height: 23px; word-break: break-word" data-color="M2 Paragraph 1" data-size="M2 Paragraph 1" data-max="26" data-min="6">
                                            Looking forward to seeing you all there on <br>
                                            Thursday, the 7th of April<br>  
          2:00 – 6:00 pm EST time<br>
          <b>Login url:</b> <a href="https://ifaacademy.virtuallive.in">https://ifaacademy.virtuallive.in/</a> <br>
          See the chart below for the conference start time by country
          
                                          </td>
                                       </tr>
                                       <!-- paragraph end -->
                                       <tr>
                                          <td height="23" style="font-size:0px">&nbsp;</td>
                                       </tr>
                                       <!-- link -->
                                       <table bgcolor="#304050" align="center"  width="900" height="500" class="margin-full ui-resizable" style="background-size: cover; background-position: center center; background-size: cover; border-radius: 6px 6px 0px 0px; background-image: 
                     url(&quot;https://test.virtuallive.in/usa1.jpg&quot;);" border="0" cellpadding="0" cellspacing="0" background="#" data-bgcolor="M1 Bgcolor 2" data-background="M1 Background 1"> </table>
                                       <!-- link end -->
                                       <tr>
                                          <td height="35" style="font-size:0px">&nbsp;</td>
                                       </tr>
                                    </tbody>
                                 </table>
                              </td>
                           </tr>
                        </tbody>
                        <div class="ui-resizable-handle ui-resizable-s" style="z-index: 90;"></div>
                     </table>
                  </td>
               </tr>
            </tbody>
         </table>
         <!-- ====== Module : Features ====== -->
         <table bgcolor="#F5F5F5" align="center" class="full" border="0" cellpadding="0" cellspacing="0" data-thumbnail="http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2020/03/22/9jhlVUzudcB8NtbnMg67SvA5/StampReady/thumbnails/thumb-3.png" data-module="Module-3" data-bgcolor="M3 Bgcolor 1">
            <tbody>
               <tr>
                  <td>
                     <table bgcolor="white" width="750" align="center" class="margin-full ui-resizable" border="0" cellpadding="0" cellspacing="0" data-bgcolor="M3 Bgcolor 2">
                        <tbody>
                           <tr>
                              <td>
                                 <table width="600" align="center" class="margin-pad" border="0" cellpadding="0" cellspacing="0">
                                    <tbody>
                                       <tr>
                                          <td height="50" style="font-size:0px">&nbsp;</td>
                                       </tr>
                                       <!-- title -->
                                       <tr>
                                          <td class="res-center" style="text-align: center; color: #000; font-family: 'Raleway', Arial, Sans-serif; font-size: 15px; letter-spacing: 0.7px; word-break: break-word" data-color="M3 Title 1" data-size="M3 Title 1" data-max="32" data-min="12">
                                           <b>Please read carefully the following Guidelines For Best Viewing Experience.</b> <br><br>
      
      1. For the best viewing experience, please login using laptop. Avoid logging in using mobile phones/tablets to get the best experience on the virtual platform. 
      <br><br>
      2. For the best experience, kindly use the Google Chrome Browser - Chrome browser is recommended for the platform as the browser policies allows engines & plugins of our platform. 
      <br><br>
      3. If you're using the office intranet/ VPN, request you to please turn it off and switch to your personal network or hotspot Some organizations have their own intranet/firewall protection for their data security which do not allow many sites to be accessed (eg: social media website, entertainment site etc) 
      <br><br>
      4. Incase the site shows blocked on your official laptop, request you to use your personal laptop as many official laptops block certain access to users for their own data security. 
      <br><br>
      5. Kindly turn off web-protection software if required. In certain instances, web protection software rejects the access to many new websites because their internal data might not be updated to whitelist the new website. 
      <br><br>
      6. Speedy & Stable Internet Connection is highly recommended. 
      <br><br>
      7. You will be required to identify yourself with your Email ID which you have used during registration, Incase you don't remember your login credentials, kindly refer to the mail which you would have received from noreply@ifaacademy.virtuallive.in 
      <br><br>
      8. Attendees joining from countries (Cambodia, Indonesia, Malaysia, Thailand, Vietnam, Philippines, and China to name a few) that do not allow certain global applications to perform in their zones. They have to use VPN to access the platform to view the conference.
      <br><br>
       <b>We hope you will enjoy the event…And don’t forget!</b>
      <br><br>
      Since you login to the platform, visit the photobooth, take an amazing shot and 
      post it at social media #ifaacademy
      
      <br><br>
      <br><br>
      
      
                                       <!-- column x2 -->
                                      
               
               <!-- paragraph -->
               <tbody>
                <tr>
                  <td valign="top" style="padding:20px 0px 10px 0px; font-size:6px; line-height:10px;" align="center">
                    <table align="center" style="-webkit-margin-start:auto;-webkit-margin-end:auto;">
                      <tbody><tr align="center"><td style="padding: 0px 5px;" class="social-icon-column">
                <a role="social-icon-link" href="https://www.facebook.com/IFAAcademyglobal" target="_blank" alt="Facebook" title="Facebook" style="display:inline-block; ">
                  <img role="social-icon" alt="Facebook" title="Facebook" src="https://test.virtuallive.in/0-05.jpg" style="height:40px; width:40px;" height="21" width="21">
                </a>
              </td><td style="padding: 0px 5px;" class="social-icon-column">
                <a role="social-icon-link" href="https://twitter.com/IFA_Academy" target="_blank" alt="Twitter" title="Twitter" style="display:inline-block;">
                  <img role="social-icon" alt="Twitter" title="Twitter" src="https://test.virtuallive.in/0-03.jpg" style="height:40px; width:40px;" height="21" width="21">
                </a>
              </td><td style="padding: 0px 5px;" class="social-icon-column">
                <a role="social-icon-link" href="https://www.instagram.com/panosleledakisifa/" target="_blank" alt="Instagram" title="Instagram" style="display:inline-block; ">
                  <img role="social-icon" alt="Instagram" title="Instagram" src="https://test.virtuallive.in/0-04.jpg" style="height:40px; width:40px;" height="21" width="21">
                </a>
              </td><td style="padding: 0px 5px;" class="social-icon-column" >
                <a role="social-icon-link" href="https://www.youtube.com/channel/UCVdR1NDFZ4g8pqqefdC3j3g" target="_blank" alt="Pinterest" title="Pinterest" style="display:inline-block;">
                  <img role="social-icon" alt="Pinterest" title="Pinterest" src="https://test.virtuallive.in/0-02.jpg" style="height:40px; width:40px;" height="21" width="21">
                </a>
              </td><td style="padding: 0px 5px;" class="social-icon-column">
                <a role="social-icon-link" href="https://www.linkedin.com/in/panos-leledakis/" target="_blank" alt="LinkedIn" title="LinkedIn" style="display:inline-block;">
                  <img role="social-icon" alt="LinkedIn" title="LinkedIn" src="https://test.virtuallive.in/0-01.jpg" style="height:40px; width:40px;" height="21" width="21">
                </a>
              </td></tr></tbody>
                    </table>
                  </td>
                </tr>
              </tbody>
               <!-- paragraph end -->
              
            </tbody>
         </table>
      </div>
      <!-- partial -->
        
      </body>
      </html>
      
  `,
    };
    sgMail
      .send(msg)
      .then(async () => {
        console.log(msg)
        let response = await newUser.save();
        res.send({response:response, redirect:TheUrl});
        })
      
    
      .catch((error) => {
        console.error(error);
        res.send({ data: "Notdone" });
      })




  }
}

if(req.body.timeZone==="For Europe-Africa-Asia-India-Australia / Friday 8 April 2022")
{
   let TheUrl= (req.body.timeZone=="For USA & Latin America / Thursday 7 April 2022"||req.body.timeZone=="For Europe-Africa-Asia-India-Australia / Friday 8 April 2022"?'https://ifaacademy.mykajabi.com/vip-session-conference-2022':'https://panagiotis-leledakis.mykajabi.com/gr-vip-session-conference')
   

   let SENDGRID_API_KEY =
   "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";
   let sgMail = require("@sendgrid/mail");
   sgMail.setApiKey(SENDGRID_API_KEY);
   console.log("europe")
    const msg = {
      to: req.body.userEmail, // Change to your recipient
      from: "noreply@ifaacademy.virtuallive.in", // Change to your verified sender
      subject: "`Thanks For resgister ",
      html: `<!DOCTYPE html>
      <html lang="en" >
      <head>
        <meta charset="UTF-8">
        <title>IFA Academy</title>
      
      
      </head>
      <body>
      <!-- partial:index.partial.html -->
      <div data-template-type="html" style="height: auto; padding-bottom: 149px;" class="ui-sortable">
         <!--[if !mso]><!-->
         <!--<![endif]-->
         <!-- ====== Module : Intro ====== -->
         <table bgcolor="#F5F5F5" align="center" class="full" border="0" cellpadding="0" cellspacing="0" data-thumbnail="http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2020/03/22/9jhlVUzudcB8NtbnMg67SvA5/StampReady/thumbnails/thumb-1.png" data-module="Module-1" data-bgcolor="M1 Bgcolor 1">
            <tbody>
               <tr>
                  <td height="25" style="font-size:0px">&nbsp;</td>
               </tr>
               <!-- top -->
               <tr>
                  <td>
                     <table align="center" class="res-full ui-resizable" border="0" cellpadding="0" cellspacing="0">
                        <tbody>
                           <tr>
                              <td>
                                 <table align="center" border="0" cellpadding="0" cellspacing="0">
                                    <tbody>
                                       <tr>
                                          <td valign="middle" style="vertical-align: middle;">
                                             <img width="18" style="max-width: 120px; width: 100%; display: block; line-height: 0px; font-size: 0px; border: 0px;" 
                                             src="https://test.virtuallive.in/ifa-logo-01.png">
                                          </td>
                                          <td width="10"></td>
                                          <td valign="middle" style="vertical-align: middle;">
                                             <table align="center" border="0" cellpadding="0" cellspacing="0">
                                                <!-- link -->
                                                <tbody>
                                                   <tr>
                                                    
                                                   </tr>
                                                   <!-- link end -->
                                                </tbody>
                                             </table>
                                          </td>
                                       </tr>
                                    </tbody>
                                 </table>
                              </td>
                           </tr>
                        </tbody>
                        <div class="ui-resizable-handle ui-resizable-s" style="z-index: 90;"></div>
                     </table>
                  </td>
               </tr>
               <!-- top end -->
               <tr>
                  <td height="25" style="font-size:0px">&nbsp;</td>
               </tr>
               <tr>
                  <td>
                     <table bgcolor="#304050" align="center"  width="900" height="400" class="margin-full ui-resizable" style="background-size: cover; background-position: center center; background-size: cover; border-radius: 6px 6px 0px 0px; background-image: 
                     url(&quot;https://test.virtuallive.in/europ.jpg&quot;);" border="0" cellpadding="0" cellspacing="0" background="#" data-bgcolor="M1 Bgcolor 2" data-background="M1 Background 1">
                        <tbody>
                           <tr>
                              <td>
                                 <table width="600" align="center" class="margin-pad" border="0" cellpadding="0" cellspacing="0">
                                    <tbody>
                                       <tr>
                                          <td height="70" style="font-size:0px">&nbsp;</td>
                                       </tr>
                                       <!-- image -->
                                       <tr>
                                          <td>
                                             <table align="center" class="res-full" border="0" cellpadding="0" cellspacing="0">
                                                <tbody>
                                                   <tr>
                                                      <td>
                                                         <table align="center" border="0" cellpadding="0" cellspacing="0">
                                                            <tbody>
                                                               <tr>
                                                                  <!-- <td>
                                                                     <img width="65" style="max-width: 65px; width: 100%; display: block; line-height: 0px; font-size: 0px; border: 0px;" src="http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2020/03/22/9jhlVUzudcB8NtbnMg67SvA5/StampReady/img/module01-img02.png">
                                                                  </td> -->
                                                               </tr>
                                                            </tbody>
                                                         </table>
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                          </td>
                                       </tr>
                                       <!-- image end -->
                                       <tr>
                                          <td height="45" style="font-size:0px">&nbsp;</td>
                                       </tr>
                                       <!-- title -->
                                       <tr>
                                       
                                       </tr>
                                       <!-- title end -->
                                       <tr>
                                          <td height="12" style="font-size:0px">&nbsp;</td>
                                       </tr>
                                       <!-- title -->
                                       <tr>
                                        
                                       </tr>
                                       <!-- title end -->
                                       <tr>
                                          <td height="30" style="font-size:0px" class="">&nbsp;</td>
                                       </tr>
                                       <tr>
                                          <td>
                                             <table align="center" border="0" cellpadding="0" cellspacing="0">
                                                <tbody>
                                                   <tr>
                                                      <td>
                                                         <table align="center" border="0" cellpadding="0" cellspacing="0">
                                                            <!-- link -->
                                                            <tbody>
                                                               <tr>
                                                               
                                                               </tr>
                                                               <!-- link end -->
                                                            </tbody>
                                                         </table>
                                                      </td>
                                                      <td style="padding: 0 12px; color: #798999;">
                                                         •
                                                      </td>
                                                      <td>
                                                         <table align="center" border="0" cellpadding="0" cellspacing="0">
                                                            <!-- link -->
                                                            <tbody>
                                                               <tr>
                                                                  <!-- <td class="res-center" style="text-align: center;">
                                                                     <a href="https://example.com" style="color: white; font-family: 'Nunito', Arial, Sans-serif; font-size: 16px; letter-spacing: 0.7px; text-decoration: none; word-break: break-word;" data-color="M1 Link 3" data-size="M1 Link 3" data-max="26" data-min="6">
                                                                     Webpage
                                                                     </a>
                                                                  </td> -->
                                                               </tr>
                                                               <!-- link end -->
                                                            </tbody>
                                                         </table>
                                                      </td>
                                                      <td style="padding: 0 12px; color: #798999;">
                                                         •
                                                      </td>
                                                      <td>
                                                         <table align="center" border="0" cellpadding="0" cellspacing="0">
                                                            <!-- link -->
                                                            <tbody>
                                                               <tr>
                                                                  <!-- <td class="res-center" style="text-align: center;">
                                                                     <a href="https://example.com" style="color: white; font-family: 'Nunito', Arial, Sans-serif; font-size: 16px; letter-spacing: 0.7px; text-decoration: none; word-break: break-word;" data-color="M1 Link 4" data-size="M1 Link 4" data-max="26" data-min="6">
                                                                     Contact
                                                                     </a>
                                                                  </td> -->
                                                               </tr>
                                                               <!-- link end -->
                                                            </tbody>
                                                         </table>
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                          </td>
                                       </tr>
                                       <tr>
                                          <td height="70" style="font-size:0px">&nbsp;</td>
                                       </tr>
                                    </tbody>
                                 </table>
                              </td>
                           </tr>
                        </tbody>
                        <div class="ui-resizable-handle ui-resizable-s" style="z-index: 90;"></div>
                     </table>
                  </td>
               </tr>
            </tbody>
         </table>
         <!-- ====== Module : Texts ====== -->
         <table bgcolor="#F5F5F5" align="center" class="full selected-table" border="0" cellpadding="0" cellspacing="0" data-thumbnail="http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2020/03/22/9jhlVUzudcB8NtbnMg67SvA5/StampReady/thumbnails/thumb-2.png" data-module="Module-2" data-bgcolor="M2 Bgcolor 1">
            <tbody>
               <tr>
                  <td>
                     <table bgcolor="white" width="750" align="center" class="margin-full ui-resizable" border="0" cellpadding="0" cellspacing="0" data-bgcolor="M2 Bgcolor 2">
                        <tbody>
                           <tr>
                              <td>
                                 <table width="600" align="center" class="margin-pad" border="0" cellpadding="0" cellspacing="0">
                                    <tbody>
                                       <tr>
                                          <td height="70" style="font-size:0px">&nbsp;</td>
                                       </tr>
                                       <!-- HEADING -->
                                       <!-- subtitle -->
                                       <tr>
                                          <td class="res-center" style="text-align: left; color: #000; font-family: 'Raleway', Arial, Sans-serif; font-size: 14px; letter-spacing: 1px; word-break: break-word; font-weight: 800;" data-color="M2 Subtitle 1" data-size="M2 Subtitle 1" data-max="24" data-min="5">
                                            Thank you so much!
                                          </td>
                                       </tr>
                                       <!-- subtitle end -->
                                       <tr>
                                          <td height="13" style="font-size:0px">&nbsp;</td>
                                       </tr>
                                       <!-- title -->
                                       <tr>
                                          <td class="res-center selected-element" style="text-align: left; color: #000; font-family: 'Raleway', Arial, Sans-serif; font-size: 15px; letter-spacing: 0.7px; word-break: break-word" data-color="M2 Title 1" data-size="M2 Title 1" data-max="32" data-min="12" contenteditable="true">
                                              We are so excited you've decided to attend our annual conference of 2022.<br>
                                              We can't wait to share our best ideas and tips and interact with you
                                              
      <br>
      <br>
                                          </td>
                                       </tr>
                              
                        
                                       <!-- image end -->
                                       <!-- HEADING end -->
                                       <tr>
                                          <td height="30" style="font-size:0px">&nbsp;</td>
                                       </tr>
                                       <!-- paragraph -->
                                       <tr>
                                          <td class="res-center" style="text-align: center; color: #000; font-family: 'Nunito', Arial, Sans-serif; font-size: 16px; letter-spacing: 0.4px; line-height: 23px; word-break: break-word" data-color="M2 Paragraph 1" data-size="M2 Paragraph 1" data-max="26" data-min="6">
                                            Looking forward to seeing you all there on <br> <br>
                                            Friday, the 8th of April  <br>  
                                         <b>11:30 am  – 3:30 pm IST time</b> <br>
      
                                         <b>2:00 – 6:00 pm ΗΚT time</b> <br>
                                         <b>7:00 - 11:00 am GMT time</b> <br><br>
      
          <b>Login url:</b> <a href="https://ifaacademy.virtuallive.in">https://ifaacademy.virtuallive.in/</a> <br> <br>
      
      
      
       <b style="color: red;">    Important notice</b> <br><br>
          Attendees joining from countries (Cambodia, Indonesia, Malaysia, Thailand, Vietnam, Philippines, and China to name a few) that do not allow certain global applications to perform in their zones. They have to use VPN to access the platform to view the conference.
          
      <br> <br>
          See the chart below for the conference start time by country
          
      
                                          </td>
                                          
                                       </tr>
                                       <!-- paragraph end -->
                                       <tr>
                                          <td height="23" style="font-size:0px">&nbsp;</td>
                                       </tr>
                                       <!-- link -->
                                       <table bgcolor="#304050" align="center"  width="900" height="500" class="margin-full ui-resizable" style="background-size: cover; background-position: center center; background-size: cover; border-radius: 6px 6px 0px 0px; background-image: 
                     url(&quot;https://test.virtuallive.in/europ1.jpg&quot;);" border="0" cellpadding="0" cellspacing="0" background="#" data-bgcolor="M1 Bgcolor 2" data-background="M1 Background 1"> </table>
                                       <!-- link end -->
                                       <tr>
                                          <td height="35" style="font-size:0px">&nbsp;</td>
                                       </tr>
                                    </tbody>
                                 </table>
                              </td>
                           </tr>
                        </tbody>
                        <div class="ui-resizable-handle ui-resizable-s" style="z-index: 90;"></div>
                     </table>
                  </td>
               </tr>
            </tbody>
         </table>
         <!-- ====== Module : Features ====== -->
         <table bgcolor="#F5F5F5" align="center" class="full" border="0" cellpadding="0" cellspacing="0" data-thumbnail="http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2020/03/22/9jhlVUzudcB8NtbnMg67SvA5/StampReady/thumbnails/thumb-3.png" data-module="Module-3" data-bgcolor="M3 Bgcolor 1">
            <tbody>
               <tr>
                  <td>
                     <table bgcolor="white" width="750" align="center" class="margin-full ui-resizable" border="0" cellpadding="0" cellspacing="0" data-bgcolor="M3 Bgcolor 2">
                        <tbody>
                           <tr>
                              <td>
                                 <table width="600" align="center" class="margin-pad" border="0" cellpadding="0" cellspacing="0">
                                    <tbody>
                                       <tr>
                                          <td height="50" style="font-size:0px">&nbsp;</td>
                                       </tr>
                                       <!-- title -->
                                       <tr>
                                          <td class="res-center" style="text-align: center; color: #000; font-family: 'Raleway', Arial, Sans-serif; font-size: 15px; letter-spacing: 0.7px; word-break: break-word" data-color="M3 Title 1" data-size="M3 Title 1" data-max="32" data-min="12">
                                           <b>Please read carefully the following Guidelines For Best Viewing Experience.</b> <br><br>
      
      1. For the best viewing experience, please login using laptop. Avoid logging in using mobile phones/tablets to get the best experience on the virtual platform. 
      <br><br>
      2. For the best experience, kindly use the Google Chrome Browser - Chrome browser is recommended for the platform as the browser policies allows engines & plugins of our platform. 
      <br><br>
      3. If you're using the office intranet/ VPN, request you to please turn it off and switch to your personal network or hotspot Some organizations have their own intranet/firewall protection for their data security which do not allow many sites to be accessed (eg: social media website, entertainment site etc) 
      <br><br>
      4. Incase the site shows blocked on your official laptop, request you to use your personal laptop as many official laptops block certain access to users for their own data security. 
      <br><br>
      5. Kindly turn off web-protection software if required. In certain instances, web protection software rejects the access to many new websites because their internal data might not be updated to whitelist the new website. 
      <br><br>
      6. Speedy & Stable Internet Connection is highly recommended. 
      <br><br>
      7. You will be required to identify yourself with your Email ID which you have used during registration, Incase you don't remember your login credentials, kindly refer to the mail which you would have received from noreply@ifaacademy.virtuallive.in 
      <br><br>
      8. Attendees joining from countries (Cambodia, Indonesia, Malaysia, Thailand, Vietnam, Philippines, and China to name a few) that do not allow certain global applications to perform in their zones. They have to use VPN to access the platform to view the conference.
      <br><br>
       <b>We hope you will enjoy the event…And don’t forget!</b>
      <br><br>
      Since you login to the platform, visit the photobooth, take an amazing shot and 
      post it at social media #ifaacademy
      
      <br><br>
      <br><br>
      
      
                                       <!-- column x2 -->
                                      
               
               <!-- paragraph -->
               <tbody>
                <tr>
                  <td valign="top" style="padding:20px 0px 10px 0px; font-size:6px; line-height:10px;" align="center">
                    <table align="center" style="-webkit-margin-start:auto;-webkit-margin-end:auto;">
                      <tbody><tr align="center"><td style="padding: 0px 5px;" class="social-icon-column">
                <a role="social-icon-link" href="https://www.facebook.com/IFAAcademyglobal" target="_blank" alt="Facebook" title="Facebook" style="display:inline-block; ">
                  <img role="social-icon" alt="Facebook" title="Facebook" src="https://test.virtuallive.in/0-05.jpg" style="height:40px; width:40px;" height="21" width="21">
                </a>
              </td><td style="padding: 0px 5px;" class="social-icon-column">
                <a role="social-icon-link" href="https://twitter.com/IFA_Academy" target="_blank" alt="Twitter" title="Twitter" style="display:inline-block;">
                  <img role="social-icon" alt="Twitter" title="Twitter" src="https://test.virtuallive.in/0-03.jpg" style="height:40px; width:40px;" height="21" width="21">
                </a>
              </td><td style="padding: 0px 5px;" class="social-icon-column">
                <a role="social-icon-link" href="https://www.instagram.com/panosleledakisifa/" target="_blank" alt="Instagram" title="Instagram" style="display:inline-block; ">
                  <img role="social-icon" alt="Instagram" title="Instagram" src="https://test.virtuallive.in/0-04.jpg" style="height:40px; width:40px;" height="21" width="21">
                </a>
              </td><td style="padding: 0px 5px;" class="social-icon-column" >
                <a role="social-icon-link" href="https://www.youtube.com/channel/UCVdR1NDFZ4g8pqqefdC3j3g" target="_blank" alt="Pinterest" title="Pinterest" style="display:inline-block;">
                  <img role="social-icon" alt="Pinterest" title="Pinterest" src="https://test.virtuallive.in/0-02.jpg" style="height:40px; width:40px;" height="21" width="21">
                </a>
              </td><td style="padding: 0px 5px;" class="social-icon-column">
                <a role="social-icon-link" href="https://www.linkedin.com/in/panos-leledakis/" target="_blank" alt="LinkedIn" title="LinkedIn" style="display:inline-block;">
                  <img role="social-icon" alt="LinkedIn" title="LinkedIn" src="https://test.virtuallive.in/0-01.jpg" style="height:40px; width:40px;" height="21" width="21">
                </a>
              </td></tr></tbody>
                    </table>
                  </td>
                </tr>
              </tbody>
               <!-- paragraph end -->
              
            </tbody>
         </table>
      </div>
      <!-- partial -->
        
      </body>
      </html>
      
 
  `,
    };
    sgMail
      .send(msg)
      .then(async () => {
        console.log(msg)
        let response = await newUser.save();
      res.send({response:response, redirect:TheUrl});
        })
      
    
      .catch((error) => {
        console.error(error);
        res.send({ data: "Notdone" });
      })




  }


  if(req.body.timeZone==="For Greece / Friday 8 April 2022")
{
   
   let TheUrl= (req.body.timeZone=="For USA & Latin America / Thursday 7 April 2022"||req.body.timeZone=="For Europe-Africa-Asia-India-Australia / Friday 8 April 2022"?'https://ifaacademy.mykajabi.com/vip-session-conference-2022':'https://panagiotis-leledakis.mykajabi.com/gr-vip-session-conference')
   

   let SENDGRID_API_KEY =
   "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";
   let sgMail = require("@sendgrid/mail");
   sgMail.setApiKey(SENDGRID_API_KEY);

    // console.log(req.body)
    // const content=req.body.imgurl
    const msg = {
      to: req.body.userEmail, // Change to your recipient
      from: "noreply@ifaacademy.virtuallive.in", // Change to your verified sender
      subject: "`Thanks For resgister ",
      html: `<!DOCTYPE html>
      <html lang="en" >
      <head>
        <meta charset="UTF-8">
        <title>IFA Academy</title>
      
      
      </head>
      <body>
      <!-- partial:index.partial.html -->
      <div data-template-type="html" style="height: auto; padding-bottom: 149px;" class="ui-sortable">
         <!--[if !mso]><!-->
         <!--<![endif]-->
         <!-- ====== Module : Intro ====== -->
         <table bgcolor="#F5F5F5" align="center" class="full" border="0" cellpadding="0" cellspacing="0" data-thumbnail="http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2020/03/22/9jhlVUzudcB8NtbnMg67SvA5/StampReady/thumbnails/thumb-1.png" data-module="Module-1" data-bgcolor="M1 Bgcolor 1">
            <tbody>
               <tr>
                  <td height="25" style="font-size:0px">&nbsp;</td>
               </tr>
               <!-- top -->
               <tr>
                  <td>
                     <table align="center" class="res-full ui-resizable" border="0" cellpadding="0" cellspacing="0">
                        <tbody>
                           <tr>
                              <td>
                                 <table align="center" border="0" cellpadding="0" cellspacing="0">
                                    <tbody>
                                       <tr>
                                          <td valign="middle" style="vertical-align: middle;">
                                             <img width="18" style="max-width: 120px; width: 100%; display: block; line-height: 0px; font-size: 0px; border: 0px;" 
                                             src="https://test.virtuallive.in/ifa-logo-01.png">
                                          </td>
                                          <td width="10"></td>
                                          <td valign="middle" style="vertical-align: middle;">
                                             <table align="center" border="0" cellpadding="0" cellspacing="0">
                                                <!-- link -->
                                                <tbody>
                                                   <tr>
                                                    
                                                   </tr>
                                                   <!-- link end -->
                                                </tbody>
                                             </table>
                                          </td>
                                       </tr>
                                    </tbody>
                                 </table>
                              </td>
                           </tr>
                        </tbody>
                        <div class="ui-resizable-handle ui-resizable-s" style="z-index: 90;"></div>
                     </table>
                  </td>
               </tr>
               <!-- top end -->
               <tr>
                  <td height="25" style="font-size:0px">&nbsp;</td>
               </tr>
               <tr>
                  <td>
                     <table bgcolor="#304050" align="center"  width="900" height="400" class="margin-full ui-resizable" style="background-size: cover; background-position: center center; background-size: cover; border-radius: 6px 6px 0px 0px; background-image: 
                     url(&quot;https://test.virtuallive.in/greek.jpg&quot;);" border="0" cellpadding="0" cellspacing="0" background="#" data-bgcolor="M1 Bgcolor 2" data-background="M1 Background 1">
                        <tbody>
                           <tr>
                              <td>
                                 <table width="600" align="center" class="margin-pad" border="0" cellpadding="0" cellspacing="0">
                                    <tbody>
                                       <tr>
                                          <td height="70" style="font-size:0px">&nbsp;</td>
                                       </tr>
                                       <!-- image -->
                                       <tr>
                                          <td>
                                             <table align="center" class="res-full" border="0" cellpadding="0" cellspacing="0">
                                                <tbody>
                                                   <tr>
                                                      <td>
                                                         <table align="center" border="0" cellpadding="0" cellspacing="0">
                                                            <tbody>
                                                               <tr>
                                                                  <!-- <td>
                                                                     <img width="65" style="max-width: 65px; width: 100%; display: block; line-height: 0px; font-size: 0px; border: 0px;" src="http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2020/03/22/9jhlVUzudcB8NtbnMg67SvA5/StampReady/img/module01-img02.png">
                                                                  </td> -->
                                                               </tr>
                                                            </tbody>
                                                         </table>
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                          </td>
                                       </tr>
                                       <!-- image end -->
                                       <tr>
                                          <td height="45" style="font-size:0px">&nbsp;</td>
                                       </tr>
                                       <!-- title -->
                                       <tr>
                                       
                                       </tr>
                                       <!-- title end -->
                                       <tr>
                                          <td height="12" style="font-size:0px">&nbsp;</td>
                                       </tr>
                                       <!-- title -->
                                       <tr>
                                        
                                       </tr>
                                       <!-- title end -->
                                       <tr>
                                          <td height="30" style="font-size:0px" class="">&nbsp;</td>
                                       </tr>
                                       <tr>
                                          <td>
                                             <table align="center" border="0" cellpadding="0" cellspacing="0">
                                                <tbody>
                                                   <tr>
                                                      <td>
                                                         <table align="center" border="0" cellpadding="0" cellspacing="0">
                                                            <!-- link -->
                                                            <tbody>
                                                               <tr>
                                                               
                                                               </tr>
                                                               <!-- link end -->
                                                            </tbody>
                                                         </table>
                                                      </td>
                                                      <td style="padding: 0 12px; color: #798999;">
                                                         •
                                                      </td>
                                                      <td>
                                                         <table align="center" border="0" cellpadding="0" cellspacing="0">
                                                            <!-- link -->
                                                            <tbody>
                                                               <tr>
                                                                  <!-- <td class="res-center" style="text-align: center;">
                                                                     <a href="https://example.com" style="color: white; font-family: 'Nunito', Arial, Sans-serif; font-size: 16px; letter-spacing: 0.7px; text-decoration: none; word-break: break-word;" data-color="M1 Link 3" data-size="M1 Link 3" data-max="26" data-min="6">
                                                                     Webpage
                                                                     </a>
                                                                  </td> -->
                                                               </tr>
                                                               <!-- link end -->
                                                            </tbody>
                                                         </table>
                                                      </td>
                                                      <td style="padding: 0 12px; color: #798999;">
                                                         •
                                                      </td>
                                                      <td>
                                                         <table align="center" border="0" cellpadding="0" cellspacing="0">
                                                            <!-- link -->
                                                            <tbody>
                                                               <tr>
                                                                  <!-- <td class="res-center" style="text-align: center;">
                                                                     <a href="https://example.com" style="color: white; font-family: 'Nunito', Arial, Sans-serif; font-size: 16px; letter-spacing: 0.7px; text-decoration: none; word-break: break-word;" data-color="M1 Link 4" data-size="M1 Link 4" data-max="26" data-min="6">
                                                                     Contact
                                                                     </a>
                                                                  </td> -->
                                                               </tr>
                                                               <!-- link end -->
                                                            </tbody>
                                                         </table>
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                          </td>
                                       </tr>
                                       <tr>
                                          <td height="70" style="font-size:0px">&nbsp;</td>
                                       </tr>
                                    </tbody>
                                 </table>
                              </td>
                           </tr>
                        </tbody>
                        <div class="ui-resizable-handle ui-resizable-s" style="z-index: 90;"></div>
                     </table>
                  </td>
               </tr>
            </tbody>
         </table>
         <!-- ====== Module : Texts ====== -->
         <table bgcolor="#F5F5F5" align="center" class="full selected-table" border="0" cellpadding="0" cellspacing="0" data-thumbnail="http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2020/03/22/9jhlVUzudcB8NtbnMg67SvA5/StampReady/thumbnails/thumb-2.png" data-module="Module-2" data-bgcolor="M2 Bgcolor 1">
            <tbody>
               <tr>
                  <td>
                     <table bgcolor="white" width="750" align="center" class="margin-full ui-resizable" border="0" cellpadding="0" cellspacing="0" data-bgcolor="M2 Bgcolor 2">
                        <tbody>
                           <tr>
                              <td>
                                 <table width="600" align="center" class="margin-pad" border="0" cellpadding="0" cellspacing="0">
                                    <tbody>
                                       <tr>
                                          <td height="70" style="font-size:0px">&nbsp;</td>
                                       </tr>
                                       <!-- HEADING -->
                                       <!-- subtitle -->
                                       <tr>
                                          <td class="res-center" style="text-align: left; color: #000; font-family: 'Raleway', Arial, Sans-serif; font-size: 14px; letter-spacing: 1px; word-break: break-word; font-weight: 800;" data-color="M2 Subtitle 1" data-size="M2 Subtitle 1" data-max="24" data-min="5">
                                              Σας ευχαριστούμε!
                                          </td>
                                       </tr>
                                       <!-- subtitle end -->
                                       <tr>
                                          <td height="13" style="font-size:0px">&nbsp;</td>
                                       </tr>
                                       <!-- title -->
                                       <tr>
                                          <td class="res-center selected-element" style="text-align: left; color: #000; font-family: 'Raleway', Arial, Sans-serif; font-size: 15px; letter-spacing: 0.7px; word-break: break-word" data-color="M2 Title 1" data-size="M2 Title 1" data-max="32" data-min="12" contenteditable="true">
                                              Είμαστε τόσο ενθουσιασμένοι που αποφασίσατε να παρευρεθείτε στο ετήσιο συνέδριό μας για το 2022. <br> Ανυπομονούμε να μοιραστούμε τις καλύτερες 
                                              ιδέες και συμβουλές μας και να αλληλεπιδράσουμε μαζί σας.
                                              
      <br>
      <br>
                                          </td>
                                       </tr>
                              
                        
                                       <!-- image end -->
                                       <!-- HEADING end -->
                                       <tr>
                                          <td height="30" style="font-size:0px">&nbsp;</td>
                                       </tr>
                                       <!-- paragraph -->
                                       <tr>
                                          <td class="res-center" style="text-align: center; color: #000; font-family: 'Nunito', Arial, Sans-serif; font-size: 16px; letter-spacing: 0.4px; line-height: 23px; word-break: break-word" data-color="M2 Paragraph 1" data-size="M2 Paragraph 1" data-max="26" data-min="6">
                                              Σας περιμένουμε την Παρασκευή 8 Απριλίου <br> <br>
                                            
                                         <b>15:00 – 19:00 ώρα Ελλάδος</b> <br>
      
      
          <b>Link εισόδου:</b> <a href="https://ifaacademy.virtuallive.in">https://ifaacademy.virtuallive.in/</a> <br> <br>
      
      
      
      
          Δείτε τον παρακάτω πίνακα με τις ώρες έναρξης ανά χώρα
          
      
                                          </td>
                                          
                                       </tr>
                                       <!-- paragraph end -->
                                       <tr>
                                          <td height="23" style="font-size:0px">&nbsp;</td>
                                       </tr>
                                       <!-- link -->
                                       <table bgcolor="#304050" align="center"  width="900" height="500" class="margin-full ui-resizable" style="background-size: cover; background-position: center center; background-size: cover; border-radius: 6px 6px 0px 0px; background-image: 
                     url(&quot;https://test.virtuallive.in/greek1.jpg&quot;);" border="0" cellpadding="0" cellspacing="0" background="#" data-bgcolor="M1 Bgcolor 2" data-background="M1 Background 1"> </table>
                                       <!-- link end -->
                                       <tr>
                                          <td height="35" style="font-size:0px">&nbsp;</td>
                                       </tr>
                                    </tbody>
                                 </table>
                              </td>
                           </tr>
                        </tbody>
                        <div class="ui-resizable-handle ui-resizable-s" style="z-index: 90;"></div>
                     </table>
                  </td>
               </tr>
            </tbody>
         </table>
         <!-- ====== Module : Features ====== -->
         <table bgcolor="#F5F5F5" align="center" class="full" border="0" cellpadding="0" cellspacing="0" data-thumbnail="http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2020/03/22/9jhlVUzudcB8NtbnMg67SvA5/StampReady/thumbnails/thumb-3.png" data-module="Module-3" data-bgcolor="M3 Bgcolor 1">
            <tbody>
               <tr>
                  <td>
                     <table bgcolor="white" width="750" align="center" class="margin-full ui-resizable" border="0" cellpadding="0" cellspacing="0" data-bgcolor="M3 Bgcolor 2">
                        <tbody>
                           <tr>
                              <td>
                                 <table width="600" align="center" class="margin-pad" border="0" cellpadding="0" cellspacing="0">
                                    <tbody>
                                       <tr>
                                          <td height="50" style="font-size:0px">&nbsp;</td>
                                       </tr>
                                       <!-- title -->
                                       <tr>
                                          <td class="res-center" style="text-align: center; color: #000; font-family: 'Raleway', Arial, Sans-serif; font-size: 15px; letter-spacing: 0.7px; word-break: break-word" data-color="M3 Title 1" data-size="M3 Title 1" data-max="32" data-min="12">
                                           <b>
                                              Παρακαλούμε διαβάστε τις παρακάτω οδηγίες για την καλύτερη δυνατή παρακολούθηση</b> <br><br>
      
                                              1. Για την καλύτερη δυνατή θέαση, παρακαλούμε συνδεθείτε χρησιμοποιώντας laptop. Αποφύγετε τη σύνδεση μέσω κινητού τηλεφώνου / tablet για να αποκτήσετε την καλύτερη εμπειρία στην εικονική πλατφόρμα.
      <br><br>
      2. Για την καλύτερη δυνατή εμπειρία, χρησιμοποιήστε το Google Chrome - Το πρόγραμμα περιήγησης Chrome συνιστάται για την πλατφόρμα, καθώς η πολιτική χρήσης του, επιτρέπει λειτουργίες και plugins της πλατφόρμας μας. 
      <br><br>
      3. Εάν χρησιμοποιείτε το intranet/ VPN του γραφείου σας, παρακαλούμε απενεργοποιήστε το και μεταβείτε στο προσωπικό σας δίκτυο ή hotspot. Ορισμένοι οργανισμοί έχουν τη δική τους προστασία intranet / firewall για την ασφάλεια των δεδομένων τους που δεν επιτρέπουν την πρόσβαση σε πολλούς ιστότοπους (π.χ. ιστότοποι κοινωνικών μέσων, ιστότοποι ψυχαγωγίας κ.λπ.) 
      <br><br>
      4. Σε περίπτωση που ο ιστότοπος είναι αποκλεισμένος στον επαγγελματικό υπολογιστή σας, χρησιμοποιήστε τον προσωπικό σας υπολογιστή, καθώς πολλοί επαγγελματικοί υπολογιστές εμποδίζουν την πρόσβαση ορισμένων χρηστών για τη δική τους ασφάλεια δεδομένων. 
      <br><br>
      5. Παρακαλούμε απενεργοποιήστε το λογισμικό προστασίας ιστού εάν απαιτείται. Σε ορισμένες περιπτώσεις, το λογισμικό προστασίας ιστού απορρίπτει την πρόσβαση σε πολλούς νέους ιστότοπους, επειδή τα εσωτερικά δεδομένα τους ενδέχεται να μην ενημερωθούν για να δώσουν πρόσβαση στον νέο ιστότοπο. 
      <br><br>
      6. Συστήνεται γρήγορη & σταθερή σύνδεση διαδικτύου. 
      <br><br>
      7. Θα σας ζητηθεί να ταυτοποιηθείτε με το email id που χρησιμοποιήσατε κατά την εγγραφή σας. Σε περίπτωση που δε θυμάστε τα στοιχεία πρόσβασής σας, παρακαλούμε ανατρέξτε στην αλληλογραφία που είχατε λάβει από noreply@ifaacademy.virtuallive.in
      
      
      <br><br>
       <b>Ελπίζουμε να απολαύσετε το συνέδριο… Και μην ξεχάσετε!</b>
      <br><br>
      Μόλις συνδεθείτε στην πλατφόρμα, επισκεφθείτε το photobooth, 
      τραβήξτε μια καταπληκτική φωτογραφία,
      και ανεβάστε τη στα μέσα κοινωνικής δικτύωσης , #ifaacademy
      
      
      <br><br>
      <br><br>
      
      
                                       <!-- column x2 -->
                                      
               
               <!-- paragraph -->
               <tbody>
                <tr>
                  <td valign="top" style="padding:20px 0px 10px 0px; font-size:6px; line-height:10px;" align="center">
                    <table align="center" style="-webkit-margin-start:auto;-webkit-margin-end:auto;">
                      <tbody><tr align="center"><td style="padding: 0px 5px;" class="social-icon-column">
                <a role="social-icon-link" href="https://www.facebook.com/IFAAcademyglobal" target="_blank" alt="Facebook" title="Facebook" style="display:inline-block; ">
                  <img role="social-icon" alt="Facebook" title="Facebook" src="https://test.virtuallive.in/0-05.jpg" style="height:40px; width:40px;" height="21" width="21">
                </a>
              </td><td style="padding: 0px 5px;" class="social-icon-column">
                <a role="social-icon-link" href="https://twitter.com/IFA_Academy" target="_blank" alt="Twitter" title="Twitter" style="display:inline-block;">
                  <img role="social-icon" alt="Twitter" title="Twitter" src="https://test.virtuallive.in/0-03.jpg" style="height:40px; width:40px;" height="21" width="21">
                </a>
              </td><td style="padding: 0px 5px;" class="social-icon-column">
                <a role="social-icon-link" href="https://www.instagram.com/panosleledakisifa/" target="_blank" alt="Instagram" title="Instagram" style="display:inline-block; ">
                  <img role="social-icon" alt="Instagram" title="Instagram" src="https://test.virtuallive.in/0-04.jpg" style="height:40px; width:40px;" height="21" width="21">
                </a>
              </td><td style="padding: 0px 5px;" class="social-icon-column" >
                <a role="social-icon-link" href="https://www.youtube.com/channel/UCVdR1NDFZ4g8pqqefdC3j3g" target="_blank" alt="Pinterest" title="Pinterest" style="display:inline-block;">
                  <img role="social-icon" alt="Pinterest" title="Pinterest" src="https://test.virtuallive.in/0-02.jpg" style="height:40px; width:40px;" height="21" width="21">
                </a>
              </td><td style="padding: 0px 5px;" class="social-icon-column">
                <a role="social-icon-link" href="https://www.linkedin.com/in/panos-leledakis/" target="_blank" alt="LinkedIn" title="LinkedIn" style="display:inline-block;">
                  <img role="social-icon" alt="LinkedIn" title="LinkedIn" src="https://test.virtuallive.in/0-01.jpg" style="height:40px; width:40px;" height="21" width="21">
                </a>
              </td></tr></tbody>
                    </table>
                  </td>
                </tr>
              </tbody>
               <!-- paragraph end -->
              
            </tbody>
         </table>
      </div>
      <!-- partial -->
        
      </body>
      </html>
      
  `,
    };
    sgMail
      .send(msg)
      .then(async () => {
        console.log(msg)
        let response = await newUser.save();
      res.send({response:response, redirect:TheUrl});
      //  res.send({response:response, redirect:TheUrl});
        })
      
    
      .catch((error) => {
        console.error(error);
        res.send({ data: "Notdone" });
      })




  }


});



app.post('/timespent', async (req, res) => {



  var newtimedata = new timeModel({

    pageid: req.body.pageid,
    time: req.body.time,
    userEmail: req.body.userEmail,

  });
  console.log("newtimedata", newtimedata);
  try {
    let response = await newtimedata.save();
    console.log("response", response);
    res.send(({ data: response }))

    return response;
  } catch (error) {
    return error;
  }
}
)



app.get("/createuser", async (req, res) => {
  console.log(
    "009999"
  )
  dataa.map(async (item) => {

    var newUser = new inforUser({
      name: item.name,
      age: item.age,
      phoneNumber: item.phoneNumber,
      status: item.status,
      email: item.email,
      token: item.token,
      password: item.password
    })
    try {
      let response = await newUser.save();
      // res.send(response);
      console.log(response);
    } catch (error) {
      return error;
    }


  }
  )
}
)




app.post("/lastlogin", async (req, res) => {
  console.log(req.body);
  let emailtest = await timeModelLogins.find({
    userEmail: req.body.userEmail,
  });
  console.log("otpTestotpTest", emailtest);
  if (emailtest && emailtest.length > 0) {
    // console.log(new Date( emailtest[0].updatedAt))

    // let date = new Date(emailtest[0].updatedAt);

    // convert Date object UNIX timestamp

    if (emailtest.length > 0) {
      timeModelLogins.findOneAndUpdate(
        {
          userEmail: req.body.userEmail,
        },
        {
          time: new Date(emailtest[0].updatedAt),
        },
        function (err, result) {
          if (err) {
            res.send(err);
          } else {
            // console.log('111www')
            res.send(result);
          }
        }
      );
    }
  } else {
    let newtime = new timeModelLogins({
      userEmail: req.body.userEmail,
      userName: req.body.userName,

      time: 3,
    });
    let response2 = await newtime.save();
    console.log("www");
    res.send(response2);
  }
});




app.get("/totalregister", async (req, res) => {
  console.log("sss", req.body);
  let emailtest = await inforUser.find({

  })

  let jsonObject = emailtest.map(JSON.stringify);

  console.log(jsonObject);

  uniqueSet = new Set(jsonObject);
  uniqueArray = Array.from(uniqueSet).map(JSON.parse);



  let count = uniqueArray.length
  let emailtestCount = emailtest.length
  res.send({ emailtestCount, count, emailtest })
}

)



function generateAccessToken(username) {
  return jwt.sign(username, require("crypto").randomBytes(64).toString("hex"));
}

app.post("/loginonetime", async (req, res) => {
  let userExist = await inforUser.findOne({
    userEmail: req.body.userEmail,
    // userName: req.body.userName,
  });

  if (userExist) {
    console.log("update");

    inforUser.findOneAndUpdate(
      {
        userEmail: req.body.userEmail,
      },
      {
        userToken: generateAccessToken(req.body.userEmail),
      },
      { new: true },
      function (err, result) {
        if (err) {
          res.send(err);
        } else {
          res.send(result);
        }
      }
    );
  } else {
    console.log("update nahi");

   
    res.status(201).send("wrongLogin");
  }
});

app.get("/registerdata", async (req, res) => {
  let userExist = await Oneusers.find({
    
  
  });
  res.send(userExist);
}
)

app.post("/registeronetime", async (req, res) => {
  let userExist = await Oneusers.findOne({
    userEmail: req.body.userEmail,
  
  });

  if (userExist) {
    console.log("update");

    res.send({data:"already resgister"});
  } else {
    console.log("update nahi");

    let storeData = await new Oneusers({
      userEmail: req.body.userEmail,
      userName: req.body.userName,
      userComp: req.body.userCompany,
      userToken: generateAccessToken(req.body.userEmail),
    });
    console.log("Oneusers",Oneusers)

    const event = {
      start: [2022, 2, 23, 14, 00],
      duration: { hours: 4, minutes: 30 },
      title: "Motion India Channel Partner Meet 2022",
      description: "Motion India Channel Partner Meet 2022",
      // location: "Folsom Field, University of Colorado (finish line)",
      // url: "http://www.bolderboulder.com/",
      // geo: { lat: 40.0095, lon: 105.2669 },
      // categories: ["10k races", "Memorial Day Weekend", "Boulder CO"],
      // status: "CONFIRMED",
      // busyStatus: "BUSY",
       organizer: { name: "ABB", email: "noreply@motionindiachannelpartnermeet2022.virtuallive.in" },
      // attendees: [
      //   {
      //     name:  req.body.userName,
      //     email:  req.body.userEmail,
      //     rsvp: true,
      //     partstat: "ACCEPTED",
      //     role: "REQ-PARTICIPANT",
      //   },
      // ],
    };
    const { value } = ics.createEvent(event);
    const SENDGRID_API_KEY =
    "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";
  const sgMail = require("@sendgrid/mail");
  sgMail.setApiKey(SENDGRID_API_KEY);


  const msg = {
    to: req.body.userEmail, // Change to your recipient
    from: "noreply@motionindiachannelpartnermeet2022.virtuallive.in", // Change to your verified sender
    subject: " Motion India Channel Partner Meet 2022 Registration ",
    html: `<!DOCTYPE html>
    <html lang="en" >
    <head>
      <meta charset="UTF-8">
      <title>CodePen - Email Newsletter Template</title>
      <link rel="stylesheet" href="./style.css">
    
    </head>
    <body>
    <!-- partial:index.partial.html -->
    <div data-template-type="html" style="height: auto; padding-bottom: 149px;" class="ui-sortable">
       <!--[if !mso]><!-->
       <!--<![endif]-->
       <!-- ====== Module : Intro ====== -->
       <table bgcolor="#F5F5F5" align="center" class="full" border="0" cellpadding="0" cellspacing="0" data-thumbnail="http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2020/03/22/9jhlVUzudcB8NtbnMg67SvA5/StampReady/thumbnails/thumb-1.png" data-module="Module-1" data-bgcolor="M1 Bgcolor 1">
          <tbody>
             <tr>
                <td height="25" style="font-size:0px">&nbsp;</td>
             </tr>
             <!-- top -->
             <tr>
                <td>
                   <table align="center" class="res-full ui-resizable" border="0" cellpadding="0" cellspacing="0">
                      <tbody>
                         <tr>
                            <td>
                               <table align="center" border="0" cellpadding="0" cellspacing="0">
                                  <tbody>
                                     <tr>
                                        <td valign="middle" style="vertical-align: middle;">
                                           <img width="18" style="max-width: 120px; width: 100%; display: block; line-height: 0px; font-size: 0px; border: 0px;" 
                                           src="https://test.virtuallive.in/logo.jpg">
                                        </td>
                                        <td width="10"></td>
                                        <td valign="middle" style="vertical-align: middle;">
                                           <table align="center" border="0" cellpadding="0" cellspacing="0">
                                              <!-- link -->
                                              <tbody>
                                                 <tr>
                                                  
                                                 </tr>
                                                 <!-- link end -->
                                              </tbody>
                                           </table>
                                        </td>
                                     </tr>
                                  </tbody>
                               </table>
                            </td>
                         </tr>
                      </tbody>
                      <div class="ui-resizable-handle ui-resizable-s" style="z-index: 90;"></div>
                   </table>
                </td>
             </tr>
             <!-- top end -->
             <tr>
                <td height="25" style="font-size:0px">&nbsp;</td>
             </tr>
             <tr>
                <td>
                   <table bgcolor="#304050" align="center"  width="900" height="400" class="margin-full ui-resizable" style="background-size: cover; background-position: center center; background-size: cover; border-radius: 6px 6px 0px 0px; background-image: 
                   url(&quot;https://test.virtuallive.in/europ.jpg&quot;);" border="0" cellpadding="0" cellspacing="0" background="#" data-bgcolor="M1 Bgcolor 2" data-background="M1 Background 1">
                      <tbody>
                         <tr>
                            <td>
                               <table width="600" align="center" class="margin-pad" border="0" cellpadding="0" cellspacing="0">
                                  <tbody>
                                     <tr>
                                        <td height="70" style="font-size:0px">&nbsp;</td>
                                     </tr>
                                     <!-- image -->
                                     <tr>
                                        <td>
                                           <table align="center" class="res-full" border="0" cellpadding="0" cellspacing="0">
                                              <tbody>
                                                 <tr>
                                                    <td>
                                                       <table align="center" border="0" cellpadding="0" cellspacing="0">
                                                          <tbody>
                                                             <tr>
                                                                <!-- <td>
                                                                   <img width="65" style="max-width: 65px; width: 100%; display: block; line-height: 0px; font-size: 0px; border: 0px;" src="http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2020/03/22/9jhlVUzudcB8NtbnMg67SvA5/StampReady/img/module01-img02.png">
                                                                </td> -->
                                                             </tr>
                                                          </tbody>
                                                       </table>
                                                    </td>
                                                 </tr>
                                              </tbody>
                                           </table>
                                        </td>
                                     </tr>
                                     <!-- image end -->
                                     <tr>
                                        <td height="45" style="font-size:0px">&nbsp;</td>
                                     </tr>
                                     <!-- title -->
                                     <tr>
                                     
                                     </tr>
                                     <!-- title end -->
                                     <tr>
                                        <td height="12" style="font-size:0px">&nbsp;</td>
                                     </tr>
                                     <!-- title -->
                                     <tr>
                                      
                                     </tr>
                                     <!-- title end -->
                                     <tr>
                                        <td height="30" style="font-size:0px" class="">&nbsp;</td>
                                     </tr>
                                     <tr>
                                        <td>
                                           <table align="center" border="0" cellpadding="0" cellspacing="0">
                                              <tbody>
                                                 <tr>
                                                    <td>
                                                       <table align="center" border="0" cellpadding="0" cellspacing="0">
                                                          <!-- link -->
                                                          <tbody>
                                                             <tr>
                                                             
                                                             </tr>
                                                             <!-- link end -->
                                                          </tbody>
                                                       </table>
                                                    </td>
                                                    <td style="padding: 0 12px; color: #798999;">
                                                       •
                                                    </td>
                                                    <td>
                                                       <table align="center" border="0" cellpadding="0" cellspacing="0">
                                                          <!-- link -->
                                                          <tbody>
                                                             <tr>
                                                                <!-- <td class="res-center" style="text-align: center;">
                                                                   <a href="https://example.com" style="color: white; font-family: 'Nunito', Arial, Sans-serif; font-size: 16px; letter-spacing: 0.7px; text-decoration: none; word-break: break-word;" data-color="M1 Link 3" data-size="M1 Link 3" data-max="26" data-min="6">
                                                                   Webpage
                                                                   </a>
                                                                </td> -->
                                                             </tr>
                                                             <!-- link end -->
                                                          </tbody>
                                                       </table>
                                                    </td>
                                                    <td style="padding: 0 12px; color: #798999;">
                                                       •
                                                    </td>
                                                    <td>
                                                       <table align="center" border="0" cellpadding="0" cellspacing="0">
                                                          <!-- link -->
                                                          <tbody>
                                                             <tr>
                                                                <!-- <td class="res-center" style="text-align: center;">
                                                                   <a href="https://example.com" style="color: white; font-family: 'Nunito', Arial, Sans-serif; font-size: 16px; letter-spacing: 0.7px; text-decoration: none; word-break: break-word;" data-color="M1 Link 4" data-size="M1 Link 4" data-max="26" data-min="6">
                                                                   Contact
                                                                   </a>
                                                                </td> -->
                                                             </tr>
                                                             <!-- link end -->
                                                          </tbody>
                                                       </table>
                                                    </td>
                                                 </tr>
                                              </tbody>
                                           </table>
                                        </td>
                                     </tr>
                                     <tr>
                                        <td height="70" style="font-size:0px">&nbsp;</td>
                                     </tr>
                                  </tbody>
                               </table>
                            </td>
                         </tr>
                      </tbody>
                      <div class="ui-resizable-handle ui-resizable-s" style="z-index: 90;"></div>
                   </table>
                </td>
             </tr>
          </tbody>
       </table>
       <!-- ====== Module : Texts ====== -->
       <table bgcolor="#F5F5F5" align="center" class="full selected-table" border="0" cellpadding="0" cellspacing="0" data-thumbnail="http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2020/03/22/9jhlVUzudcB8NtbnMg67SvA5/StampReady/thumbnails/thumb-2.png" data-module="Module-2" data-bgcolor="M2 Bgcolor 1">
          <tbody>
             <tr>
                <td>
                   <table bgcolor="white" width="750" align="center" class="margin-full ui-resizable" border="0" cellpadding="0" cellspacing="0" data-bgcolor="M2 Bgcolor 2">
                      <tbody>
                         <tr>
                            <td>
                               <table width="600" align="center" class="margin-pad" border="0" cellpadding="0" cellspacing="0">
                                  <tbody>
                                     <tr>
                                        <td height="70" style="font-size:0px">&nbsp;</td>
                                     </tr>
                                     <!-- HEADING -->
                                     <!-- subtitle -->
                                     <tr>
                                        <td class="res-center" style="text-align: left; color: #000; font-family: 'Raleway', Arial, Sans-serif; font-size: 14px; letter-spacing: 1px; word-break: break-word; font-weight: 800;" data-color="M2 Subtitle 1" data-size="M2 Subtitle 1" data-max="24" data-min="5">
                                          Thank you so much!
                                        </td>
                                     </tr>
                                     <!-- subtitle end -->
                                     <tr>
                                        <td height="13" style="font-size:0px">&nbsp;</td>
                                     </tr>
                                     <!-- title -->
                                     <tr>
                                        <td class="res-center selected-element" style="text-align: center; color: #000; font-family: 'Raleway', Arial, Sans-serif; font-size: 15px; letter-spacing: 0.7px; word-break: break-word" data-color="M2 Title 1" data-size="M2 Title 1" data-max="32" data-min="12" contenteditable="true">
                                          We are so excited you've decided to attend our annual conference of 2022.
    We can't wait to share our best ideas and tips and interact with you.
    
                                        </td>
                                     </tr>
                                     <!-- title end -->
                                     <tr>
                                        <td height="15" style="font-size:0px">&nbsp;</td>
                                     </tr>
                                     <!-- image -->
                                     <tr>
                                        <td>
                                           <table align="center" class="res-full" border="0" cellpadding="0" cellspacing="0">
                                              <tbody>
                                                 <tr>
                                                    <td>
                                                       <table align="center" border="0" cellpadding="0" cellspacing="0">
                                                          <tbody>
                                                             <tr>
                                                                <!-- <td>
                                                                   <img width="130" style="max-width: 130px; width: 100%; display: block; line-height: 0px; font-size: 0px; border: 0px;" src="http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2020/03/22/9jhlVUzudcB8NtbnMg67SvA5/StampReady/img/ui-line-2.png">
                                                                </td> -->
                                                             </tr>
                                                          </tbody>
                                                       </table>
                                                    </td>
                                                 </tr>
                                              </tbody>
                                           </table>
                                        </td>
                                     </tr>
                                     <!-- image end -->
                                     <!-- HEADING end -->
                                     <tr>
                                        <td height="30" style="font-size:0px">&nbsp;</td>
                                     </tr>
                                     <!-- paragraph -->
                                     <tr>
                                        <td class="res-center" style="text-align: center; color: #000; font-family: 'Nunito', Arial, Sans-serif; font-size: 16px; letter-spacing: 0.4px; line-height: 23px; word-break: break-word" data-color="M2 Paragraph 1" data-size="M2 Paragraph 1" data-max="26" data-min="6">
                                          Looking forward to seeing you all there on <br>
                                          Thursday, the 7th of April<br>  
        2:00 – 6:00 pm EST time<br>
        <b>Login url:</b> <a href="https://ifaacademy.virtuallive.in">https://ifaacademy.virtuallive.in/</a> <br>
        See the chart below for the conference start time by country
        
                                        </td>
                                     </tr>
                                     <!-- paragraph end -->
                                     <tr>
                                        <td height="23" style="font-size:0px">&nbsp;</td>
                                     </tr>
                                     <!-- link -->
                                     <table bgcolor="#304050" align="center"  width="900" height="500" class="margin-full ui-resizable" style="background-size: cover; background-position: center center; background-size: cover; border-radius: 6px 6px 0px 0px; background-image: 
                   url(&quot;https://test.virtuallive.in/europ1.jpg&quot;);" border="0" cellpadding="0" cellspacing="0" background="#" data-bgcolor="M1 Bgcolor 2" data-background="M1 Background 1"> </table>
                                     <!-- link end -->
                                     <tr>
                                        <td height="35" style="font-size:0px">&nbsp;</td>
                                     </tr>
                                  </tbody>
                               </table>
                            </td>
                         </tr>
                      </tbody>
                      <div class="ui-resizable-handle ui-resizable-s" style="z-index: 90;"></div>
                   </table>
                </td>
             </tr>
          </tbody>
       </table>
       <!-- ====== Module : Features ====== -->
       <table bgcolor="#F5F5F5" align="center" class="full" border="0" cellpadding="0" cellspacing="0" data-thumbnail="http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2020/03/22/9jhlVUzudcB8NtbnMg67SvA5/StampReady/thumbnails/thumb-3.png" data-module="Module-3" data-bgcolor="M3 Bgcolor 1">
          <tbody>
             <tr>
                <td>
                   <table bgcolor="white" width="750" align="center" class="margin-full ui-resizable" border="0" cellpadding="0" cellspacing="0" data-bgcolor="M3 Bgcolor 2">
                      <tbody>
                         <tr>
                            <td>
                               <table width="600" align="center" class="margin-pad" border="0" cellpadding="0" cellspacing="0">
                                  <tbody>
                                     <tr>
                                        <td height="50" style="font-size:0px">&nbsp;</td>
                                     </tr>
                                     <!-- title -->
                                     <tr>
                                        <td class="res-center" style="text-align: center; color: #000; font-family: 'Raleway', Arial, Sans-serif; font-size: 15px; letter-spacing: 0.7px; word-break: break-word" data-color="M3 Title 1" data-size="M3 Title 1" data-max="32" data-min="12">
                                         <b>Please read carefully the following Guidelines For Best Viewing Experience.</b> <br><br>
    
    1. For the best viewing experience, please login using laptop. Avoid logging in using mobile phones/tablets to get the best experience on the virtual platform. 
    <br><br>
    2. For the best experience, kindly use the Google Chrome Browser - Chrome browser is recommended for the platform as the browser policies allows engines & plugins of our platform. 
    <br><br>
    3. If you're using the office intranet/ VPN, request you to please turn it off and switch to your personal network or hotspot Some organizations have their own intranet/firewall protection for their data security which do not allow many sites to be accessed (eg: social media website, entertainment site etc) 
    <br><br>
    4. Incase the site shows blocked on your official laptop, request you to use your personal laptop as many official laptops block certain access to users for their own data security. 
    <br><br>
    5. Kindly turn off web-protection software if required. In certain instances, web protection software rejects the access to many new websites because their internal data might not be updated to whitelist the new website. 
    <br><br>
    6. Speedy & Stable Internet Connection is highly recommended. 
    <br><br>
    7. You will be required to identify yourself with your Email ID which you have used during registration, Incase you don't remember your login credentials, kindly refer to the mail which you would have received from noreply@ifaacademy.virtuallive.in 
    <br><br>
    8. Attendees joining from countries (Cambodia, Indonesia, Malaysia, Thailand, Vietnam, Philippines, and China to name a few) that do not allow certain global applications to perform in their zones. They have to use VPN to access the platform to view the conference.
    <br><br>
     <b>We hope you will enjoy the event…And don’t forget!</b>
    <br><br>
    Since you login to the platform, visit the photobooth, take an amazing shot and 
    post it at social media #ifaacademy
    
    <br><br>
    <br><br>
    
    
                                     <!-- column x2 -->
                                    
             
             <!-- paragraph -->
             <tbody>
              <tr>
                <td valign="top" style="padding:20px 0px 10px 0px; font-size:6px; line-height:10px;" align="center">
                  <table align="center" style="-webkit-margin-start:auto;-webkit-margin-end:auto;">
                    <tbody><tr align="center"><td style="padding: 0px 5px;" class="social-icon-column">
              <a role="social-icon-link" href="https://www.facebook.com/IFAAcademyglobal" target="_blank" alt="Facebook" title="Facebook" style="display:inline-block; background-color:#2b6175; height:21px; width:21px;">
                <img role="social-icon" alt="Facebook" title="Facebook" src="https://mc.sendgrid.com/assets/social/white/facebook.png" style="height:21px; width:21px;" height="21" width="21">
              </a>
            </td><td style="padding: 0px 5px;" class="social-icon-column">
              <a role="social-icon-link" href="https://twitter.com/IFA_Academy" target="_blank" alt="Twitter" title="Twitter" style="display:inline-block; background-color:#2b6175; height:21px; width:21px;">
                <img role="social-icon" alt="Twitter" title="Twitter" src="https://mc.sendgrid.com/assets/social/white/twitter.png" style="height:21px; width:21px;" height="21" width="21">
              </a>
            </td><td style="padding: 0px 5px;" class="social-icon-column">
              <a role="social-icon-link" href="https://www.instagram.com/panosleledakisifa/" target="_blank" alt="Instagram" title="Instagram" style="display:inline-block; background-color:#2b6175; height:21px; width:21px;">
                <img role="social-icon" alt="Instagram" title="Instagram" src="https://mc.sendgrid.com/assets/social/white/instagram.png" style="height:21px; width:21px;" height="21" width="21">
              </a>
            </td><td style="padding: 0px 5px; background-color: #2b6175;" class="social-icon-column" >
              <a role="social-icon-link" href="https://www.pinterest.com/sendgrid/" target="_blank" alt="Pinterest" title="Pinterest" style="display:inline-block; background-color:#2b6175; height:21px; width:21px;">
                <img role="social-icon" alt="Pinterest" title="Pinterest" src="https://www.google.com/imgres?imgurl=https%3A%2F%2Fwww.iconpacks.net%2Ficons%2F1%2Ffree-youtube-icon-105-thumb.png&imgrefurl=https%3A%2F%2Fwww.iconpacks.net%2Ffree-icon%2Fyoutube-105.html&tbnid=MT7FwMjpZPtZ7M&vet=12ahUKEwi2xY_VuPr2AhWHk9gFHTXaAg8QMygfegUIARCHAg..i&docid=t_IwBuMqoH3FoM&w=512&h=512&q=youtube%20white%20icon&ved=2ahUKEwi2xY_VuPr2AhWHk9gFHTXaAg8QMygfegUIARCHAg" style="height:21px; width:21px;" height="21" width="21">
              </a>
            </td><td style="padding: 0px 5px;" class="social-icon-column">
              <a role="social-icon-link" href="https://www.linkedin.com/in/panos-leledakis/" target="_blank" alt="LinkedIn" title="LinkedIn" style="display:inline-block; background-color:#2b6175; height:21px; width:21px;">
                <img role="social-icon" alt="LinkedIn" title="LinkedIn" src="https://mc.sendgrid.com/assets/social/white/linkedin.png" style="height:21px; width:21px;" height="21" width="21">
              </a>
            </td></tr></tbody>
                  </table>
                </td>
              </tr>
            </tbody>
             <!-- paragraph end -->
            
          </tbody>
       </table>
    </div>
    <!-- partial -->
      
    </body>
    </html>
    
`,

  };
  sgMail
    .send(msg)
    .then(async () => {

   
      let sendData = await storeData.save();
      res.send({data:"changed",msg:msg});
  
    })
    .catch((error) => {
      console.error(error);
      res.send({ data: "Notdone" });
    })





   
    // res.send(sendData);
  }
});

app.post("/getoken", async (req, res) => {
  let userExistToken = await Oneusers.findOne({
    userEmail: req.body.userEmail,
    userToken: req.body.userToken,
  });

  if (userExistToken) {
    console.log("update");

    res.send("true");
  } else {
    res.send("false");
  }
});






app.post("/popuplogin", async (req, res) => {
  console.log(req.body);
  let emailtest = await timeModelLogins.findOne({
    userEmail: req.body.userEmail,
  })
  if (emailtest) {
    res.send('true')
  }
  else {
    res.send('false')
  }
}
)



app.post("/likescountsmiles", async (req, res) => {
  console.log(req.body);
  let emailtest = await timeModelLogins.findOne({
    userEmail: req.body.userEmail,
  })
  if (emailtest) {
    res.send('true')
  }
  else {
    res.send('false')
  }
}
)


app.post("/likescounttell", async (req, res) => {
   console.log(req.body);
   let dataFind1 = await Likesuser.findOne({userEmail:'sppl.it2@gmail.com'})   
   if (dataFind1) {
      console.log(dataFind1)
     res.send(dataFind1)
   }}
 )
 


app.post("/likescounthit", async (req, res) => {

  // let storeData = await new Likesuser({
  //   userEmail:'sppl.it2@gmail.com'
   
  // });

  // let sendData = await storeData.save();


if(req.body.smileyname=="noone"){
  let dataFind1 = await Likesuser.findOne({userEmail:'sppl.it2@gmail.com'})   
  console.log(dataFind1)  
  res.send(dataFind1) 
}
else{
 const dataFind = await Likesuser.findOne({userEmail:'sppl.it2@gmail.com'})   
 console.log(dataFind)   
 
 let  smileyHeart=dataFind.smileyHeart ;
 let smileythumb=dataFind.smileythumb
 let smileyClap=dataFind.smileyClap


  Likesuser.findOneAndUpdate(
    {
      userEmail:'sppl.it2@gmail.com',
    },
    {
     
       
   smileyHeart:req.body.smileyname=="smileyHeart"? dataFind.smileyHeart+Math.floor(Math.random() * 10)+1 :dataFind.smileyHeart,
  smileythumb:req.body.smileyname=="smileythumb"? dataFind.smileythumb+Math.floor(Math.random() * 10)+1 :dataFind.smileythumb,
  smileyClap:req.body.smileyname=="smileyClap"? dataFind.smileyClap+Math.floor(Math.random() * 10)+1 :dataFind.smileyClap,
    },
    { new: true },
    function (err, result) {
      if (err) {
        res.send(err);
      } else {
        res.send(result);
      }
    }
  )
}
}
)


app.get("/enterdata", async (req, res) => {


  let storeData = await new Oneusers({
    userEmail: 'sppl.it2@gmail.com',
    userName: 'Gaurav Chauhan',
    userToken:  'kjjk',
  });
  
  let sendData = await storeData.save();
  
  res.send(sendData)
  })








  app.post("/registernews", async (req, res) => {
    let userExist = await Oneusers.findOne({userName: req.body.userName})
    console.log(req.body,userExist)
  
    if (userExist) {
      console.log("update2222");
  
      res.send({data:"true"});
    } else {
      console.log("update nahi22");
  
      let storeData = await new Oneusers({
        userEmail: req.body.userEmail||"",
        userName: req.body.userName||"",
        userComp: req.body.userCompany||"",
        userToken: generateAccessToken(req.body.userName),
      })
      res.send({data:storeData});
    
    }

  }
  )


//   app.get("/mailsending", async (req,res)=>{

//     const SENDGRID_API_KEY =
//       "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";
//     const sgMail = require("@sendgrid/mail");
//     sgMail.setApiKey(SENDGRID_API_KEY);
//     // console.log(req.body)
//     // const content=req.body.imgurl
//     const msg = {
//       to: "sppl.it2@gmail.com", // Change to your recipient
//       from: "noreply@ifaacademy.virtuallive.in", // Change to your verified sender
//       subject: "`Thanks For resgister ",
//       html: `<!DOCTYPE html>
//       <html lang="en" >
//       <head>
//         <meta charset="UTF-8">
//         <title>CodePen - Email Newsletter Template</title>
//         <link rel="stylesheet" href="./style.css">
      
//       </head>
//       <body>
//       <!-- partial:index.partial.html -->
//       <div data-template-type="html" style="height: auto; padding-bottom: 149px;" class="ui-sortable">
//          <!--[if !mso]><!-->
//          <!--<![endif]-->
//          <!-- ====== Module : Intro ====== -->
//          <table bgcolor="#F5F5F5" align="center" class="full" border="0" cellpadding="0" cellspacing="0" data-thumbnail="http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2020/03/22/9jhlVUzudcB8NtbnMg67SvA5/StampReady/thumbnails/thumb-1.png" data-module="Module-1" data-bgcolor="M1 Bgcolor 1">
//             <tbody>
//                <tr>
//                   <td height="25" style="font-size:0px">&nbsp;</td>
//                </tr>
//                <!-- top -->
//                <tr>
//                   <td>
//                      <table align="center" class="res-full ui-resizable" border="0" cellpadding="0" cellspacing="0">
//                         <tbody>
//                            <tr>
//                               <td>
//                                  <table align="center" border="0" cellpadding="0" cellspacing="0">
//                                     <tbody>
//                                        <tr>
//                                           <td valign="middle" style="vertical-align: middle;">
//                                              <img width="18" style="max-width: 120px; width: 100%; display: block; line-height: 0px; font-size: 0px; border: 0px;" 
//                                              src="https://test.virtuallive.in/logo.jpg">
//                                           </td>
//                                           <td width="10"></td>
//                                           <td valign="middle" style="vertical-align: middle;">
//                                              <table align="center" border="0" cellpadding="0" cellspacing="0">
//                                                 <!-- link -->
//                                                 <tbody>
//                                                    <tr>
                                                    
//                                                    </tr>
//                                                    <!-- link end -->
//                                                 </tbody>
//                                              </table>
//                                           </td>
//                                        </tr>
//                                     </tbody>
//                                  </table>
//                               </td>
//                            </tr>
//                         </tbody>
//                         <div class="ui-resizable-handle ui-resizable-s" style="z-index: 90;"></div>
//                      </table>
//                   </td>
//                </tr>
//                <!-- top end -->
//                <tr>
//                   <td height="25" style="font-size:0px">&nbsp;</td>
//                </tr>
//                <tr>
//                   <td>
//                      <table bgcolor="#304050" align="center"  width="900" height="400" class="margin-full ui-resizable" style="background-size: cover; background-position: center center; background-size: cover; border-radius: 6px 6px 0px 0px; background-image: 
//                      url(&quot;https://test.virtuallive.in/europ.jpg&quot;);" border="0" cellpadding="0" cellspacing="0" background="#" data-bgcolor="M1 Bgcolor 2" data-background="M1 Background 1">
//                         <tbody>
//                            <tr>
//                               <td>
//                                  <table width="600" align="center" class="margin-pad" border="0" cellpadding="0" cellspacing="0">
//                                     <tbody>
//                                        <tr>
//                                           <td height="70" style="font-size:0px">&nbsp;</td>
//                                        </tr>
//                                        <!-- image -->
//                                        <tr>
//                                           <td>
//                                              <table align="center" class="res-full" border="0" cellpadding="0" cellspacing="0">
//                                                 <tbody>
//                                                    <tr>
//                                                       <td>
//                                                          <table align="center" border="0" cellpadding="0" cellspacing="0">
//                                                             <tbody>
//                                                                <tr>
//                                                                   <!-- <td>
//                                                                      <img width="65" style="max-width: 65px; width: 100%; display: block; line-height: 0px; font-size: 0px; border: 0px;" src="http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2020/03/22/9jhlVUzudcB8NtbnMg67SvA5/StampReady/img/module01-img02.png">
//                                                                   </td> -->
//                                                                </tr>
//                                                             </tbody>
//                                                          </table>
//                                                       </td>
//                                                    </tr>
//                                                 </tbody>
//                                              </table>
//                                           </td>
//                                        </tr>
//                                        <!-- image end -->
//                                        <tr>
//                                           <td height="45" style="font-size:0px">&nbsp;</td>
//                                        </tr>
//                                        <!-- title -->
//                                        <tr>
                                       
//                                        </tr>
//                                        <!-- title end -->
//                                        <tr>
//                                           <td height="12" style="font-size:0px">&nbsp;</td>
//                                        </tr>
//                                        <!-- title -->
//                                        <tr>
                                        
//                                        </tr>
//                                        <!-- title end -->
//                                        <tr>
//                                           <td height="30" style="font-size:0px" class="">&nbsp;</td>
//                                        </tr>
//                                        <tr>
//                                           <td>
//                                              <table align="center" border="0" cellpadding="0" cellspacing="0">
//                                                 <tbody>
//                                                    <tr>
//                                                       <td>
//                                                          <table align="center" border="0" cellpadding="0" cellspacing="0">
//                                                             <!-- link -->
//                                                             <tbody>
//                                                                <tr>
                                                               
//                                                                </tr>
//                                                                <!-- link end -->
//                                                             </tbody>
//                                                          </table>
//                                                       </td>
//                                                       <td style="padding: 0 12px; color: #798999;">
//                                                          •
//                                                       </td>
//                                                       <td>
//                                                          <table align="center" border="0" cellpadding="0" cellspacing="0">
//                                                             <!-- link -->
//                                                             <tbody>
//                                                                <tr>
//                                                                   <!-- <td class="res-center" style="text-align: center;">
//                                                                      <a href="https://example.com" style="color: white; font-family: 'Nunito', Arial, Sans-serif; font-size: 16px; letter-spacing: 0.7px; text-decoration: none; word-break: break-word;" data-color="M1 Link 3" data-size="M1 Link 3" data-max="26" data-min="6">
//                                                                      Webpage
//                                                                      </a>
//                                                                   </td> -->
//                                                                </tr>
//                                                                <!-- link end -->
//                                                             </tbody>
//                                                          </table>
//                                                       </td>
//                                                       <td style="padding: 0 12px; color: #798999;">
//                                                          •
//                                                       </td>
//                                                       <td>
//                                                          <table align="center" border="0" cellpadding="0" cellspacing="0">
//                                                             <!-- link -->
//                                                             <tbody>
//                                                                <tr>
//                                                                   <!-- <td class="res-center" style="text-align: center;">
//                                                                      <a href="https://example.com" style="color: white; font-family: 'Nunito', Arial, Sans-serif; font-size: 16px; letter-spacing: 0.7px; text-decoration: none; word-break: break-word;" data-color="M1 Link 4" data-size="M1 Link 4" data-max="26" data-min="6">
//                                                                      Contact
//                                                                      </a>
//                                                                   </td> -->
//                                                                </tr>
//                                                                <!-- link end -->
//                                                             </tbody>
//                                                          </table>
//                                                       </td>
//                                                    </tr>
//                                                 </tbody>
//                                              </table>
//                                           </td>
//                                        </tr>
//                                        <tr>
//                                           <td height="70" style="font-size:0px">&nbsp;</td>
//                                        </tr>
//                                     </tbody>
//                                  </table>
//                               </td>
//                            </tr>
//                         </tbody>
//                         <div class="ui-resizable-handle ui-resizable-s" style="z-index: 90;"></div>
//                      </table>
//                   </td>
//                </tr>
//             </tbody>
//          </table>
//          <!-- ====== Module : Texts ====== -->
//          <table bgcolor="#F5F5F5" align="center" class="full selected-table" border="0" cellpadding="0" cellspacing="0" data-thumbnail="http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2020/03/22/9jhlVUzudcB8NtbnMg67SvA5/StampReady/thumbnails/thumb-2.png" data-module="Module-2" data-bgcolor="M2 Bgcolor 1">
//             <tbody>
//                <tr>
//                   <td>
//                      <table bgcolor="white" width="750" align="center" class="margin-full ui-resizable" border="0" cellpadding="0" cellspacing="0" data-bgcolor="M2 Bgcolor 2">
//                         <tbody>
//                            <tr>
//                               <td>
//                                  <table width="600" align="center" class="margin-pad" border="0" cellpadding="0" cellspacing="0">
//                                     <tbody>
//                                        <tr>
//                                           <td height="70" style="font-size:0px">&nbsp;</td>
//                                        </tr>
//                                        <!-- HEADING -->
//                                        <!-- subtitle -->
//                                        <tr>
//                                           <td class="res-center" style="text-align: left; color: #000; font-family: 'Raleway', Arial, Sans-serif; font-size: 14px; letter-spacing: 1px; word-break: break-word; font-weight: 800;" data-color="M2 Subtitle 1" data-size="M2 Subtitle 1" data-max="24" data-min="5">
//                                             Thank you so much!
//                                           </td>
//                                        </tr>
//                                        <!-- subtitle end -->
//                                        <tr>
//                                           <td height="13" style="font-size:0px">&nbsp;</td>
//                                        </tr>
//                                        <!-- title -->
//                                        <tr>
//                                           <td class="res-center selected-element" style="text-align: center; color: #000; font-family: 'Raleway', Arial, Sans-serif; font-size: 15px; letter-spacing: 0.7px; word-break: break-word" data-color="M2 Title 1" data-size="M2 Title 1" data-max="32" data-min="12" contenteditable="true">
//                                             We are so excited you've decided to attend our annual conference of 2022.
//       We can't wait to share our best ideas and tips and interact with you.
      
//                                           </td>
//                                        </tr>
//                                        <!-- title end -->
//                                        <tr>
//                                           <td height="15" style="font-size:0px">&nbsp;</td>
//                                        </tr>
//                                        <!-- image -->
//                                        <tr>
//                                           <td>
//                                              <table align="center" class="res-full" border="0" cellpadding="0" cellspacing="0">
//                                                 <tbody>
//                                                    <tr>
//                                                       <td>
//                                                          <table align="center" border="0" cellpadding="0" cellspacing="0">
//                                                             <tbody>
//                                                                <tr>
//                                                                   <!-- <td>
//                                                                      <img width="130" style="max-width: 130px; width: 100%; display: block; line-height: 0px; font-size: 0px; border: 0px;" src="http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2020/03/22/9jhlVUzudcB8NtbnMg67SvA5/StampReady/img/ui-line-2.png">
//                                                                   </td> -->
//                                                                </tr>
//                                                             </tbody>
//                                                          </table>
//                                                       </td>
//                                                    </tr>
//                                                 </tbody>
//                                              </table>
//                                           </td>
//                                        </tr>
//                                        <!-- image end -->
//                                        <!-- HEADING end -->
//                                        <tr>
//                                           <td height="30" style="font-size:0px">&nbsp;</td>
//                                        </tr>
//                                        <!-- paragraph -->
//                                        <tr>
//                                           <td class="res-center" style="text-align: center; color: #000; font-family: 'Nunito', Arial, Sans-serif; font-size: 16px; letter-spacing: 0.4px; line-height: 23px; word-break: break-word" data-color="M2 Paragraph 1" data-size="M2 Paragraph 1" data-max="26" data-min="6">
//                                             Looking forward to seeing you all there on <br>
//                                             Thursday, the 7th of April<br>  
//           2:00 – 6:00 pm EST time<br>
//           <b>Login url:</b> <a href="https://ifaacademy.virtuallive.in">https://ifaacademy.virtuallive.in/</a> <br>
//           See the chart below for the conference start time by country
          
//                                           </td>
//                                        </tr>
//                                        <!-- paragraph end -->
//                                        <tr>
//                                           <td height="23" style="font-size:0px">&nbsp;</td>
//                                        </tr>
//                                        <!-- link -->
//                                        <table bgcolor="#304050" align="center"  width="900" height="500" class="margin-full ui-resizable" style="background-size: cover; background-position: center center; background-size: cover; border-radius: 6px 6px 0px 0px; background-image: 
//                      url(&quot;https://test.virtuallive.in/europ1.jpg&quot;);" border="0" cellpadding="0" cellspacing="0" background="#" data-bgcolor="M1 Bgcolor 2" data-background="M1 Background 1"> </table>
//                                        <!-- link end -->
//                                        <tr>
//                                           <td height="35" style="font-size:0px">&nbsp;</td>
//                                        </tr>
//                                     </tbody>
//                                  </table>
//                               </td>
//                            </tr>
//                         </tbody>
//                         <div class="ui-resizable-handle ui-resizable-s" style="z-index: 90;"></div>
//                      </table>
//                   </td>
//                </tr>
//             </tbody>
//          </table>
//          <!-- ====== Module : Features ====== -->
//          <table bgcolor="#F5F5F5" align="center" class="full" border="0" cellpadding="0" cellspacing="0" data-thumbnail="http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2020/03/22/9jhlVUzudcB8NtbnMg67SvA5/StampReady/thumbnails/thumb-3.png" data-module="Module-3" data-bgcolor="M3 Bgcolor 1">
//             <tbody>
//                <tr>
//                   <td>
//                      <table bgcolor="white" width="750" align="center" class="margin-full ui-resizable" border="0" cellpadding="0" cellspacing="0" data-bgcolor="M3 Bgcolor 2">
//                         <tbody>
//                            <tr>
//                               <td>
//                                  <table width="600" align="center" class="margin-pad" border="0" cellpadding="0" cellspacing="0">
//                                     <tbody>
//                                        <tr>
//                                           <td height="50" style="font-size:0px">&nbsp;</td>
//                                        </tr>
//                                        <!-- title -->
//                                        <tr>
//                                           <td class="res-center" style="text-align: center; color: #000; font-family: 'Raleway', Arial, Sans-serif; font-size: 15px; letter-spacing: 0.7px; word-break: break-word" data-color="M3 Title 1" data-size="M3 Title 1" data-max="32" data-min="12">
//                                            <b>Please read carefully the following Guidelines For Best Viewing Experience.</b> <br><br>
      
//       1. For the best viewing experience, please login using laptop. Avoid logging in using mobile phones/tablets to get the best experience on the virtual platform. 
//       <br><br>
//       2. For the best experience, kindly use the Google Chrome Browser - Chrome browser is recommended for the platform as the browser policies allows engines & plugins of our platform. 
//       <br><br>
//       3. If you're using the office intranet/ VPN, request you to please turn it off and switch to your personal network or hotspot Some organizations have their own intranet/firewall protection for their data security which do not allow many sites to be accessed (eg: social media website, entertainment site etc) 
//       <br><br>
//       4. Incase the site shows blocked on your official laptop, request you to use your personal laptop as many official laptops block certain access to users for their own data security. 
//       <br><br>
//       5. Kindly turn off web-protection software if required. In certain instances, web protection software rejects the access to many new websites because their internal data might not be updated to whitelist the new website. 
//       <br><br>
//       6. Speedy & Stable Internet Connection is highly recommended. 
//       <br><br>
//       7. You will be required to identify yourself with your Email ID which you have used during registration, Incase you don't remember your login credentials, kindly refer to the mail which you would have received from noreply@ifaacademy.virtuallive.in 
//       <br><br>
//       8. Attendees joining from countries (Cambodia, Indonesia, Malaysia, Thailand, Vietnam, Philippines, and China to name a few) that do not allow certain global applications to perform in their zones. They have to use VPN to access the platform to view the conference.
//       <br><br>
//        <b>We hope you will enjoy the event…And don’t forget!</b>
//       <br><br>
//       Since you login to the platform, visit the photobooth, take an amazing shot and 
//       post it at social media #ifaacademy
      
//       <br><br>
//       <br><br>
      
      
//                                        <!-- column x2 -->
                                      
               
//                <!-- paragraph -->
//                <tbody>
//                 <tr>
//                   <td valign="top" style="padding:20px 0px 10px 0px; font-size:6px; line-height:10px;" align="center">
//                     <table align="center" style="-webkit-margin-start:auto;-webkit-margin-end:auto;">
//                       <tbody><tr align="center"><td style="padding: 0px 5px;" class="social-icon-column">
//                 <a role="social-icon-link" href="https://www.facebook.com/IFAAcademyglobal" target="_blank" alt="Facebook" title="Facebook" style="display:inline-block; background-color:#2b6175; height:21px; width:21px;">
//                   <img role="social-icon" alt="Facebook" title="Facebook" src="https://mc.sendgrid.com/assets/social/white/facebook.png" style="height:21px; width:21px;" height="21" width="21">
//                 </a>
//               </td><td style="padding: 0px 5px;" class="social-icon-column">
//                 <a role="social-icon-link" href="https://twitter.com/IFA_Academy" target="_blank" alt="Twitter" title="Twitter" style="display:inline-block; background-color:#2b6175; height:21px; width:21px;">
//                   <img role="social-icon" alt="Twitter" title="Twitter" src="https://mc.sendgrid.com/assets/social/white/twitter.png" style="height:21px; width:21px;" height="21" width="21">
//                 </a>
//               </td><td style="padding: 0px 5px;" class="social-icon-column">
//                 <a role="social-icon-link" href="https://www.instagram.com/panosleledakisifa/" target="_blank" alt="Instagram" title="Instagram" style="display:inline-block; background-color:#2b6175; height:21px; width:21px;">
//                   <img role="social-icon" alt="Instagram" title="Instagram" src="https://mc.sendgrid.com/assets/social/white/instagram.png" style="height:21px; width:21px;" height="21" width="21">
//                 </a>
//               </td><td style="padding: 0px 5px; background-color: #2b6175;" class="social-icon-column" >
//                 <a role="social-icon-link" href="https://www.pinterest.com/sendgrid/" target="_blank" alt="Pinterest" title="Pinterest" style="display:inline-block; background-color:#2b6175; height:21px; width:21px;">
//                   <img role="social-icon" alt="Pinterest" title="Pinterest" src="https://www.google.com/imgres?imgurl=https%3A%2F%2Fwww.iconpacks.net%2Ficons%2F1%2Ffree-youtube-icon-105-thumb.png&imgrefurl=https%3A%2F%2Fwww.iconpacks.net%2Ffree-icon%2Fyoutube-105.html&tbnid=MT7FwMjpZPtZ7M&vet=12ahUKEwi2xY_VuPr2AhWHk9gFHTXaAg8QMygfegUIARCHAg..i&docid=t_IwBuMqoH3FoM&w=512&h=512&q=youtube%20white%20icon&ved=2ahUKEwi2xY_VuPr2AhWHk9gFHTXaAg8QMygfegUIARCHAg" style="height:21px; width:21px;" height="21" width="21">
//                 </a>
//               </td><td style="padding: 0px 5px;" class="social-icon-column">
//                 <a role="social-icon-link" href="https://www.linkedin.com/in/panos-leledakis/" target="_blank" alt="LinkedIn" title="LinkedIn" style="display:inline-block; background-color:#2b6175; height:21px; width:21px;">
//                   <img role="social-icon" alt="LinkedIn" title="LinkedIn" src="https://mc.sendgrid.com/assets/social/white/linkedin.png" style="height:21px; width:21px;" height="21" width="21">
//                 </a>
//               </td></tr></tbody>
//                     </table>
//                   </td>
//                 </tr>
//               </tbody>
//                <!-- paragraph end -->
              
//             </tbody>
//          </table>
//       </div>
//       <!-- partial -->
        
//       </body>
//       </html>
      
//  `,
//     };
//     sgMail
//       .send(msg)
//       .then(async () => {
//         console.log(msg)
        

//         res.send("ssss");
//         })
      
    
//       .catch((error) => {
//         console.error(error);
//         res.send({ data: "Notdone" });
//       })




  
//   })