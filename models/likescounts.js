const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const Likesuser = new Schema(
    {
        smileyHeart: Number,
        userEmail: String,

        smileythumb: Number,
        smileyClap: Number


    },
    {
        timestamps: true
    }
);

module.exports = mongoose.model("likescounts", Likesuser);