const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const Oneusers = new Schema(
  {
    firstName: String,
   
    lastName: String,
    userEmail:String,
    userCountryName:String,
    mobileNumber:String,
    telCode:String,
    timeZone:String,
    companyName:String,
    dob:String,
    bType:String,
    userToken:String
  
    
  },
  {
    timestamps: true
  }
);

module.exports = mongoose.model("oneusers", Oneusers);

// First Name
// First Name
// Last Name
// Last Name
// Email
// Email
// Country Name